package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the genre database table.
 * 
 */
@Entity
@NamedQuery(name="Genre.findAll", query="SELECT g FROM Genre g")
public class Genre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String name;

	//bi-directional many-to-one association to AlbumGenre
	@OneToMany(mappedBy="genreBean")
	private List<AlbumGenre> albumGenres;

	//bi-directional many-to-one association to ArtistGenre
	@OneToMany(mappedBy="genreBean")
	private List<ArtistGenre> artistGenres;

	public Genre() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AlbumGenre> getAlbumGenres() {
		return this.albumGenres;
	}

	public void setAlbumGenres(List<AlbumGenre> albumGenres) {
		this.albumGenres = albumGenres;
	}

	public AlbumGenre addAlbumGenre(AlbumGenre albumGenre) {
		getAlbumGenres().add(albumGenre);
		albumGenre.setGenreBean(this);

		return albumGenre;
	}

	public AlbumGenre removeAlbumGenre(AlbumGenre albumGenre) {
		getAlbumGenres().remove(albumGenre);
		albumGenre.setGenreBean(null);

		return albumGenre;
	}

	public List<ArtistGenre> getArtistGenres() {
		return this.artistGenres;
	}

	public void setArtistGenres(List<ArtistGenre> artistGenres) {
		this.artistGenres = artistGenres;
	}

	public ArtistGenre addArtistGenre(ArtistGenre artistGenre) {
		getArtistGenres().add(artistGenre);
		artistGenre.setGenreBean(this);

		return artistGenre;
	}

	public ArtistGenre removeArtistGenre(ArtistGenre artistGenre) {
		getArtistGenres().remove(artistGenre);
		artistGenre.setGenreBean(null);

		return artistGenre;
	}

}