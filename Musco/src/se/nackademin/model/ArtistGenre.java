package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the artist_genres database table.
 * 
 */
@Entity
@Table(name="artist_genres")
@NamedQuery(name="ArtistGenre.findAll", query="SELECT a FROM ArtistGenre a")
public class ArtistGenre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Artist
	@ManyToOne
	@JoinColumn(name="artist")
	private Artist artistBean;

	//bi-directional many-to-one association to Genre
	@ManyToOne
	@JoinColumn(name="genre")
	private Genre genreBean;

	public ArtistGenre() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Artist getArtistBean() {
		return this.artistBean;
	}

	public void setArtistBean(Artist artistBean) {
		this.artistBean = artistBean;
	}

	public Genre getGenreBean() {
		return this.genreBean;
	}

	public void setGenreBean(Genre genreBean) {
		this.genreBean = genreBean;
	}

}