package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the rating database table.
 * 
 */
@Embeddable
public class RatingPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private String user;

	@Column(insertable=false, updatable=false)
	private int album;

	public RatingPK() {
	}
	public String getUser() {
		return this.user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getAlbum() {
		return this.album;
	}
	public void setAlbum(int album) {
		this.album = album;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RatingPK)) {
			return false;
		}
		RatingPK castOther = (RatingPK)other;
		return 
			this.user.equals(castOther.user)
			&& (this.album == castOther.album);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.user.hashCode();
		hash = hash * prime + this.album;
		
		return hash;
	}
}