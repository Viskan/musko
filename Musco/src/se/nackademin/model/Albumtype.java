package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the albumtype database table.
 * 
 */
@Entity
@NamedQuery(name="Albumtype.findAll", query="SELECT a FROM Albumtype a")
public class Albumtype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String name;

	//bi-directional many-to-one association to Album
	@OneToMany(mappedBy="albumtype")
	private List<Album> albums;

	public Albumtype() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Album> getAlbums() {
		return this.albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	public Album addAlbum(Album album) {
		getAlbums().add(album);
		album.setAlbumtype(this);

		return album;
	}

	public Album removeAlbum(Album album) {
		getAlbums().remove(album);
		album.setAlbumtype(null);

		return album;
	}

}