package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the album_genres database table.
 * 
 */
@Entity
@Table(name="album_genres")
@NamedQuery(name="AlbumGenre.findAll", query="SELECT a FROM AlbumGenre a")
public class AlbumGenre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Album
	@ManyToOne
	@JoinColumn(name="album")
	private Album albumBean;

	//bi-directional many-to-one association to Genre
	@ManyToOne
	@JoinColumn(name="genre")
	private Genre genreBean;

	public AlbumGenre() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Album getAlbumBean() {
		return this.albumBean;
	}

	public void setAlbumBean(Album albumBean) {
		this.albumBean = albumBean;
	}

	public Genre getGenreBean() {
		return this.genreBean;
	}

	public void setGenreBean(Genre genreBean) {
		this.genreBean = genreBean;
	}

}