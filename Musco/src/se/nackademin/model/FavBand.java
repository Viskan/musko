package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the fav_bands database table.
 * 
 */
@Entity
@Table(name="fav_bands")
@NamedQuery(name="FavBand.findAll", query="SELECT f FROM FavBand f")
public class FavBand implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Artist
	@ManyToOne
	@JoinColumn(name="artist")
	private Artist artistBean;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user")
	private User userBean;

	public FavBand() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Artist getArtistBean() {
		return this.artistBean;
	}

	public void setArtistBean(Artist artistBean) {
		this.artistBean = artistBean;
	}

	public User getUserBean() {
		return this.userBean;
	}

	public void setUserBean(User userBean) {
		this.userBean = userBean;
	}

}