package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the artist database table.
 * 
 */
@Entity
@NamedQuery(name="Artist.findAll", query="SELECT a FROM Artist a")
public class Artist implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String description;

	private String image;

	private String name;

	@Column(name="spotify_id")
	private String spotifyId;

	//bi-directional many-to-one association to Album
	@OneToMany(mappedBy="artistBean")
	private List<Album> albums;

	//bi-directional many-to-many association to User
	@ManyToMany
	@JoinTable(
		name="fav_bands"
		, joinColumns={
			@JoinColumn(name="artist")
			}
		, inverseJoinColumns={
			@JoinColumn(name="user")
			}
		)
	private List<User> users;

	//bi-directional many-to-one association to ArtistGenre
	@OneToMany(mappedBy="artistBean")
	private List<ArtistGenre> artistGenres;

	//bi-directional many-to-one association to FavBand
	@OneToMany(mappedBy="artistBean")
	private List<FavBand> favBands;

	public Artist() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpotifyId() {
		return this.spotifyId;
	}

	public void setSpotifyId(String spotifyId) {
		this.spotifyId = spotifyId;
	}

	public List<Album> getAlbums() {
		return this.albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	public Album addAlbum(Album album) {
		getAlbums().add(album);
		album.setArtistBean(this);

		return album;
	}

	public Album removeAlbum(Album album) {
		getAlbums().remove(album);
		album.setArtistBean(null);

		return album;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<ArtistGenre> getArtistGenres() {
		return this.artistGenres;
	}

	public void setArtistGenres(List<ArtistGenre> artistGenres) {
		this.artistGenres = artistGenres;
	}

	public ArtistGenre addArtistGenre(ArtistGenre artistGenre) {
		getArtistGenres().add(artistGenre);
		artistGenre.setArtistBean(this);

		return artistGenre;
	}

	public ArtistGenre removeArtistGenre(ArtistGenre artistGenre) {
		getArtistGenres().remove(artistGenre);
		artistGenre.setArtistBean(null);

		return artistGenre;
	}

	public List<FavBand> getFavBands() {
		return this.favBands;
	}

	public void setFavBands(List<FavBand> favBands) {
		this.favBands = favBands;
	}

	public FavBand addFavBand(FavBand favBand) {
		getFavBands().add(favBand);
		favBand.setArtistBean(this);

		return favBand;
	}

	public FavBand removeFavBand(FavBand favBand) {
		getFavBands().remove(favBand);
		favBand.setArtistBean(null);

		return favBand;
	}

}