package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the album database table.
 * 
 */
@Entity
@NamedQuery(name="Album.findAll", query="SELECT a FROM Album a")
public class Album implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String image;

	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name="release_date")
	private Date releaseDate;

	@Column(name="spotify_id")
	private String spotifyId;

	//bi-directional many-to-one association to Albumtype
	@ManyToOne
	@JoinColumn(name="type")
	private Albumtype albumtype;

	//bi-directional many-to-one association to Artist
	@ManyToOne
	@JoinColumn(name="artist")
	private Artist artistBean;

	//bi-directional many-to-one association to AlbumGenre
	@OneToMany(mappedBy="albumBean")
	private List<AlbumGenre> albumGenres;

	//bi-directional many-to-one association to Rating
	@OneToMany(mappedBy="albumBean")
	private List<Rating> ratings;

	//bi-directional many-to-one association to Review
	@OneToMany(mappedBy="albumBean")
	private List<Review> reviews;

	public Album() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getSpotifyId() {
		return this.spotifyId;
	}

	public void setSpotifyId(String spotifyId) {
		this.spotifyId = spotifyId;
	}

	public Albumtype getAlbumtype() {
		return this.albumtype;
	}

	public void setAlbumtype(Albumtype albumtype) {
		this.albumtype = albumtype;
	}

	public Artist getArtistBean() {
		return this.artistBean;
	}

	public void setArtistBean(Artist artistBean) {
		this.artistBean = artistBean;
	}

	public List<AlbumGenre> getAlbumGenres() {
		return this.albumGenres;
	}

	public void setAlbumGenres(List<AlbumGenre> albumGenres) {
		this.albumGenres = albumGenres;
	}

	public AlbumGenre addAlbumGenre(AlbumGenre albumGenre) {
		getAlbumGenres().add(albumGenre);
		albumGenre.setAlbumBean(this);

		return albumGenre;
	}

	public AlbumGenre removeAlbumGenre(AlbumGenre albumGenre) {
		getAlbumGenres().remove(albumGenre);
		albumGenre.setAlbumBean(null);

		return albumGenre;
	}

	public List<Rating> getRatings() {
		return this.ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public Rating addRating(Rating rating) {
		getRatings().add(rating);
		rating.setAlbumBean(this);

		return rating;
	}

	public Rating removeRating(Rating rating) {
		getRatings().remove(rating);
		rating.setAlbumBean(null);

		return rating;
	}

	public List<Review> getReviews() {
		return this.reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public Review addReview(Review review) {
		getReviews().add(review);
		review.setAlbumBean(this);

		return review;
	}

	public Review removeReview(Review review) {
		getReviews().remove(review);
		review.setAlbumBean(null);

		return review;
	}

}