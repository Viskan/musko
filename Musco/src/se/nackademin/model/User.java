package se.nackademin.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String name;

	private String image;

	private String mail;

	private String password;

	@Column(name="user_level")
	private int userLevel;

	//bi-directional many-to-many association to Artist
	@ManyToMany(mappedBy="users")
	private List<Artist> artists;

	//bi-directional many-to-one association to Comment
	@OneToMany(mappedBy="userBean")
	private List<Comment> comments;

	//bi-directional many-to-one association to FavBand
	@OneToMany(mappedBy="userBean")
	private List<FavBand> favBands;

	//bi-directional many-to-one association to Rating
	@OneToMany(mappedBy="userBean")
	private List<Rating> ratings;

	//bi-directional many-to-one association to Review
	@OneToMany(mappedBy="userBean")
	private List<Review> reviews;

	public User() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUserLevel() {
		return this.userLevel;
	}

	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}

	public List<Artist> getArtists() {
		return this.artists;
	}

	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	public List<Comment> getComments() {
		return this.comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Comment addComment(Comment comment) {
		getComments().add(comment);
		comment.setUserBean(this);

		return comment;
	}

	public Comment removeComment(Comment comment) {
		getComments().remove(comment);
		comment.setUserBean(null);

		return comment;
	}

	public List<FavBand> getFavBands() {
		return this.favBands;
	}

	public void setFavBands(List<FavBand> favBands) {
		this.favBands = favBands;
	}

	public FavBand addFavBand(FavBand favBand) {
		getFavBands().add(favBand);
		favBand.setUserBean(this);

		return favBand;
	}

	public FavBand removeFavBand(FavBand favBand) {
		getFavBands().remove(favBand);
		favBand.setUserBean(null);

		return favBand;
	}

	public List<Rating> getRatings() {
		return this.ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public Rating addRating(Rating rating) {
		getRatings().add(rating);
		rating.setUserBean(this);

		return rating;
	}

	public Rating removeRating(Rating rating) {
		getRatings().remove(rating);
		rating.setUserBean(null);

		return rating;
	}

	public List<Review> getReviews() {
		return this.reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public Review addReview(Review review) {
		getReviews().add(review);
		review.setUserBean(this);

		return review;
	}

	public Review removeReview(Review review) {
		getReviews().remove(review);
		review.setUserBean(null);

		return review;
	}

}