package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import se.nackademin.model.Review;

@Stateless
public class ReviewManager {


	@PersistenceContext
	EntityManager em;
	
	public void saveReview(Review newReview){
		em.merge(newReview);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Review> getAllReviews(){
		
		List<Review> review;
		Query q = em.createQuery("SELECT review FROM Review review ORDER BY review.albumBean.artistBean.name ASC");
		 
		try{
			review = (List<Review>) q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		
		return review;
	}
	
	public Review getReviewById(String id){
		
		Review review;
		int parsedString = Integer.parseInt(id);
		Query q = em.createQuery("SELECT review FROM Review review WHERE review.id=?1");
		q.setParameter(1, parsedString);
		
		try{
			review = (Review) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		
		return review;
	}
	
	public List<Review> getReviewsByAlbum(String id){
		
		List<Review> review;
		int parsedString = Integer.parseInt(id);
		Query q = em.createQuery("SELECT review FROM Review review WHERE review.albumBean.id=?1");
		q.setParameter(1, parsedString);
		
		try{
			review = (List<Review>) q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		
		return review;
	}
	
	@SuppressWarnings("unchecked")
	public List<Review> getReviewSortByDate(int limit){
		
		Query q = em.createQuery("SELECT review FROM Review review ORDER BY review.date DESC");
		q.setMaxResults(limit);
		
		List<Review> review = (List<Review>) q.getResultList();
		
		return review;
	}
	
	public Review getReviewByUser(String userName){
		
		Review review;
		Query q = em.createQuery("SELECT review FROM Review review WHERE review.userBean.name=?1 ORDER BY review.id DESC");
		q.setMaxResults(1);
		q.setParameter(1, userName);
		try{
			review = (Review) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		
		return review;
	}
	
@SuppressWarnings("unchecked")
public List<Review> getReviewsByUser(String userName){
	List<Review> reviews;
		Query q = em.createQuery("SELECT review FROM Review review WHERE review.userBean.name=?1 ORDER BY review.date DESC");
		q.setParameter(1, userName);
		try{
		reviews = (List<Review>) q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return reviews;
	}
	
}
