package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import se.nackademin.model.Album;
import se.nackademin.model.Artist;

@Stateless
public class ArtistManager {

	@PersistenceContext
	EntityManager em;
	
	public List<Artist> getAllArtists(){
		
		Query q = em.createQuery("SELECT artist FROM Artist artist ORDER BY artist.name");
		List<Artist> artistList = q.getResultList();
		
		return artistList;
	}
	public List<Artist> getAllRecentArtists(int limit){
		
		Query q = em.createQuery("SELECT artist FROM Artist artist ORDER BY artist.id DESC");
		q.setMaxResults(limit);
		List<Artist> artistList = q.getResultList();
		
		return artistList;
	}
	
	public Artist getArtistById(String id){

		Artist artist;

		int parsedString = Integer.parseInt(id);
		
		Query q = em.createQuery("SELECT artist FROM Artist artist WHERE artist.id=?1");
		q.setParameter(1, parsedString);
		
		try{
			artist = (Artist) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return artist;
	}
	
	@SuppressWarnings("unchecked")
	public List<Artist> getArtistsBySearch(String search){
		
		List<Artist> artistList;
		Query q = em.createQuery("SELECT artist FROM Artist artist WHERE artist.name LIKE '"+search+"%'");
		
		try{
			artistList = (List<Artist>) q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return artistList;
	}
	
	public Artist findArtistByExactMatch(String search){
		
		Artist artist;
		Query q = em.createQuery("SELECT artist FROM Artist artist WHERE artist.name=?1");
		q.setParameter(1, search);
		try{
			artist = (Artist) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		
		return artist;
	}
	
	public void saveArtist(Artist artist){
		em.merge(artist);
	}
	
	public Artist findArtistBySpotifyID(String id){

		try{
			Query q = em.createQuery("SELECT artist FROM Artist artist WHERE artist.spotifyId = ?1");
			q.setParameter(1, id);
			Artist a = (Artist) q.getSingleResult();
			Artist b = new Artist();
			b.setId(a.getId());
			b.setImage(a.getImage());
			b.setSpotifyId(a.getSpotifyId());
			b.setDescription(a.getDescription());
			b.setName(a.getName());
			
			return b;
		}catch(NoResultException e){
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Artist> getNewestArtistsByLimit(int limit){
		
		List<Artist> artistList;
		Query q = em.createQuery("SELECT artist FROM Artist artist ORDER BY artist.id DESC");
		q.setMaxResults(limit);
		try{
			artistList = (List<Artist>) q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return artistList;
	}
	
	public List<Artist> getArtistByGenre(String name, int limit) {
		
		List<Artist> artists;
		Query q = em.createQuery("SELECT artist FROM Artist artist JOIN artist.artistGenres ag JOIN ag.genreBean gb WHERE gb.name=?1");
		q.setParameter(1, name);
		q.setMaxResults(limit);

		try{
			artists = q.getResultList();
		}catch(NoResultException e){
			artists = null;
		}
		
		return artists;
	}
	
}
