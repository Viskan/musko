package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import se.nackademin.model.Artist;
import se.nackademin.model.ArtistGenre;
import se.nackademin.model.Genre;

@Stateless
public class GenreManager {

	@PersistenceContext
	EntityManager em;
	private Artist artist;
	
	@SuppressWarnings("unchecked")
	public List<Genre> getAllGenres(String s){
		
		Query q = em.createQuery("SELECT genre FROM Genre genre WHERE genre.name LIKE '"+s+"%'");
		List<Genre> genre = (List<Genre>) q.getResultList();
		return genre;
	}
	
	@SuppressWarnings("unchecked")
	public List<ArtistGenre> getArtistsByGenre(String g){
		
		Query q = em.createQuery("SELECT artist FROM ArtistGenre artist WHERE artist.genreBean.name=?1 ORDER BY artist.genreBean.name");
		q.setParameter(1, g);
		
		return (List<ArtistGenre>) q.getResultList();
	}
}
