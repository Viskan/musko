package se.nackademin.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import se.nackademin.model.User;
import se.nackademin.utility.SHA256;

@Stateless
public class userManager {

	@PersistenceContext
	EntityManager em;
	SHA256 hash;
	
	
	public User findUser(User user){
		
		hash = new SHA256(user.getPassword());
		String password = hash.getHashValue();
		
		Query q = em.createQuery("SELECT user FROM User user WHERE user.name='" + user.getName() + "' AND user.password='" + password + "'");
		try{
			user = (User) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return user;
	}
	
	public boolean validateUsername(String username){
		
		Query q = em.createQuery("SELECT user FROM User user where user.name= :username");
		try{
			q.setParameter("username", username).getSingleResult();
		}catch(NoResultException e){
			return true;
		}
		return false;
	}
	
	public void registerUser(User user){
		em.persist(user);
	}
	
	public User getUser(String userName){
		User user;
		Query q = em.createQuery("SELECT user FROM User user where user.name=?1");
		q.setParameter(1, userName);
		try{
			user = (User) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return user;
	}
}
