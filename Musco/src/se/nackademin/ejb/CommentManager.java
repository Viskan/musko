package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;


import javax.persistence.Query;

import se.nackademin.model.Artist;
import se.nackademin.model.Comment;

@Stateless
public class CommentManager {

	@PersistenceContext
	EntityManager em;


	public void saveComment(Comment comment){
		em.merge(comment);
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getCommentsByReview(String id){
		int intID = Integer.parseInt(id);
		List<Comment> commentList;
		Query q = em.createQuery("SELECT comment FROM Comment comment WHERE comment.reviewBean.id=?1");
		q.setParameter(1, intID);
		try{
			commentList = (List<Comment>) q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return commentList;
	}

	public List<Comment> getLatestComments(int limit) {

		List<Comment> commentList;
		Query q = em.createQuery("SELECT comment FROM Comment comment ORDER BY comment.date DESC");
		q.setMaxResults(limit);
		try{
			commentList = (List<Comment>)q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return commentList;
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getCommentsByUser(String userName){
		List<Comment> comments;
		Query q = em.createQuery("SELECT comment FROM Comment comment WHERE comment.userBean.name=?1 ORDER BY comment.date");
		q.setParameter(1, userName);
		try{
			comments = (List<Comment>)q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return comments;
	}


}
