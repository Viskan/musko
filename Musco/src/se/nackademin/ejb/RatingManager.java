package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import se.nackademin.model.Rating;

@Stateless
public class RatingManager {

	@PersistenceContext
	EntityManager em;
	
	public void saveRating(Rating rating){
		
		em.merge(rating);
	}
	
	public Rating findRatingByAlbumAndUser(String albumId, String user){
		
		int parsedId = Integer.parseInt(albumId);
		Rating rating;
		Query q = em.createQuery("SELECT rating FROM Rating rating WHERE rating.userBean.name=?1 AND rating.albumBean.id=?2");
		q.setParameter(1, user);
		q.setParameter(2, parsedId);
		
		try{
			rating = (Rating)q.getSingleResult();
		}catch(NoResultException e){
			rating = null;
		}
		return rating;
	}

	public List<Rating> findRatingsByAlbum(int id) {
		
		List<Rating> ratings;
		Query q = em.createQuery("SELECT rating from Rating rating WHERE rating.albumBean.id=?1");
		q.setParameter(1, id);
		
		try{
			ratings = q.getResultList();
		}catch(NoResultException e){
			ratings = null;
		}
		return ratings;
	}
	
	public List<Rating> getAllRatingsByUser(String userName){
		List<Rating> ratings;
		Query q = em.createQuery("SELECT rating from Rating rating WHERE rating.userBean.name=?1 ORDER BY rating.date");
		q.setParameter(1, userName);
		try{
			ratings = q.getResultList();
		}catch(NoResultException e){
			ratings = null;
		}
		return ratings;
	}
	
	public List<Rating> getLatestRatings(int limit){
		List<Rating> ratings;
		Query q = em.createQuery("SELECT rating from Rating rating ORDER BY rating.date DESC");
		q.setMaxResults(limit);
		try{
			ratings = q.getResultList();
		}catch(NoResultException e){
			ratings = null;
		}return ratings;
	}
}
