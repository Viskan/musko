package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;




import javax.persistence.Query;

import se.nackademin.model.FavBand;

@Stateless
public class FavBandManager {

	@PersistenceContext
	EntityManager em;


	public void deleteFavBand(FavBand favBand){
		
		em.remove(em.merge(favBand));
	}

	public void saveFavBand(FavBand favBand){
		em.merge(favBand);
	}

	@SuppressWarnings("unchecked")
	public List<FavBand> getFavBandsByArtist(String id){
		int intID = Integer.parseInt(id);
		List<FavBand> favBands; 
		Query q = em.createQuery("SELECT favBand FROM FavBand favBand WHERE favBand.artistBean.id=?1");
		q.setParameter(1, intID);
		try{
			favBands = q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return favBands;
	}

	public FavBand getFavband(FavBand favB){

		Query q = em.createQuery("SELECT favBand FROM FavBand favBand WHERE favBand.artistBean.id='" + favB.getArtistBean().getId() + "' AND favBand.userBean.name='" + favB.getUserBean().getName() + "'");
		try{
			favB = (FavBand) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		return favB;
	}
	
	@SuppressWarnings("unchecked")
	public List<FavBand> getFavBandsByUser(String userName){
		List<FavBand> favBands;
		Query q = em.createQuery("SELECT favBand FROM FavBand favBand WHERE favBand.userBean.name=?1");
		q.setParameter(1, userName);
		try{
			favBands = q.getResultList();
		}catch(NoResultException e){
			return null;
		}
		return favBands;
	}

}


