package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import se.nackademin.model.FavBand;
import se.nackademin.model.Rating;

@Stateless
public class TopListManager {
	
	@PersistenceContext
	EntityManager em;
	
@SuppressWarnings("unchecked")
public List<FavBand> getFavBands() {
	
		List<FavBand> favBands;
		Query q = em.createQuery("SELECT favBand from FavBand favBand");
		//q.setMaxResults(limit);
		
		try{
			favBands = q.getResultList();
			
		}catch(NoResultException e){
			favBands = null;
		}
		
		//for(FavBand f: favBands){
		
			
		//}
		return favBands;
	}

	
	

}
