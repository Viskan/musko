package se.nackademin.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import se.nackademin.model.Album;
import se.nackademin.model.Albumtype;

@Stateless
public class AlbumManager {

	@PersistenceContext
	EntityManager em;

	public void saveAlbum(Album album){
		em.merge(album);
	}
	
	public Album findAlbumById(String id){
		
		Album album;
		int parsedString = Integer.parseInt(id);
		Query q = em.createQuery("SELECT album FROM Album album WHERE album.id = ?1");
		q.setParameter(1, parsedString);
		
		try{
			album = (Album)q.getSingleResult();
		}catch(NoResultException e){
			album = null;
		}
		
		return album;
	}
	
@SuppressWarnings("unchecked")
public List<Album> findAlbumsByArtist(String artist){
		
		List<Album> albums;
		int parsedString = Integer.parseInt(artist);
		Query q = em.createQuery("SELECT album FROM Album album WHERE album.artistBean.id = ?1 ORDER BY album.releaseDate DESC");
		q.setParameter(1, parsedString);
		
		try{
			albums = q.getResultList();
		}catch(NoResultException e){
			albums = null;
		}
		
		return albums;
	}
	

	public Album findAlbumBySpotifyID(String id){
	
		try{
			Query q = em.createQuery("SELECT album FROM Album album WHERE album.spotifyId = ?1");
			q.setParameter(1, id);
			Album a = (Album) q.getSingleResult();
			Album b = new Album();
			b.setId(a.getId());
			b.setImage(a.getImage());
			b.setName(a.getName());
			b.setReleaseDate(a.getReleaseDate());
			b.setSpotifyId(a.getSpotifyId());
			return b;
			
			
		}catch(NoResultException e){
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Albumtype> getAllAlbumTypes(){
		Query q = em.createNamedQuery("Albumtype.findAll");
		
		return (List<Albumtype>)q.getResultList();
		
	}
	
	public Album getAlbumById(String id){
		
		Album idAlbum;
		int parsedString = Integer.parseInt(id);
		Query q = em.createQuery("SELECT album FROM Album album WHERE album.id=?1");
		q.setParameter(1, parsedString);
	
		try{
		idAlbum = (Album) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
		
		return idAlbum;
	}
	
	public List<Album> findAlbumsByGenre(String genre, int limit){
		
		List<Album> albums;
		Query q = em.createQuery("SELECT album FROM Album album JOIN  album.albumGenres ag JOIN ag.genreBean gb WHERE gb.name=?1");
		q.setParameter(1, genre);
		q.setMaxResults(limit);
		try{
			albums = q.getResultList();
		}catch(NoResultException e){
			albums = null;
		}
		
		return albums;
		
	}

}
