package se.nackademin.controller;

import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.AlbumManager;
import se.nackademin.ejb.ReviewManager;
import se.nackademin.model.Album;
import se.nackademin.model.Review;
import se.nackademin.model.User;


@WebServlet("/AddReview")
public class AddReview extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Inject
	AlbumManager am;
	@Inject
	ReviewManager rm;
 
    public AddReview() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String albumId = request.getParameter("byid");
		
		Album album = am.getAlbumById(albumId);
		request.setAttribute("album", album);
		
		if(album == null){
			request.getRequestDispatcher("WEB-INF/views/artists.jsp").forward(request, response);
		}
		request.getRequestDispatcher("WEB-INF/views/addReview.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if(session.getAttribute("member") != null){
			User u = (User) session.getAttribute("member");
			String reviewText = request.getParameter("enteredText");
			String albumId = request.getParameter("albumId");
			
			Date date = new Date();
			
			Review r = new Review();
			r.setText(reviewText);
			r.setDate(date);
			r.setAlbumBean(am.getAlbumById(albumId));
			
			r.setUserBean(u);
			rm.saveReview(r);
	
			String reviewUser = request.getParameter("byuser");
			Review review = rm.getReviewByUser(reviewUser);
			
			response.sendRedirect("Review?byid="+review.getId());
		}
	}

}
