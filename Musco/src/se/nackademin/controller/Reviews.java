package se.nackademin.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.nackademin.ejb.ReviewManager;
import se.nackademin.model.Review;

@WebServlet("/Reviews")
public class Reviews extends HttpServlet {
	
	@Inject
	ReviewManager rm;
	
	private static final long serialVersionUID = 1L;
       
    public Reviews() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		List<Review>reviews = rm.getAllReviews();
		request.setAttribute("allReviews", reviews);
		
		List<Review>reviewsByDate = rm.getReviewSortByDate(8);
		request.setAttribute("reviewsSortedByDate", reviewsByDate);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/views/reviews.jsp");
		view.forward(request, response);
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
