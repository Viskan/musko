package se.nackademin.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.nackademin.ejb.ArtistManager;
import se.nackademin.ejb.RatingManager;
import se.nackademin.ejb.ReviewManager;
import se.nackademin.model.Artist;
import se.nackademin.model.Rating;
import se.nackademin.model.Review;

@WebServlet("/News")
public class News extends HttpServlet {
	private static final long serialVersionUID = 1L;
     @Inject
     ArtistManager am;
     @Inject
     ReviewManager rm;
     @Inject
     RatingManager rtm;
     
    public News() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		List<Artist> artistList = am.getNewestArtistsByLimit(10);
		request.setAttribute("artistList", artistList);
		List<Review> reviewList = rm.getReviewSortByDate(20);
		request.setAttribute("reviewList", reviewList);
		List<Rating> ratingList = rtm.getLatestRatings(20);
		request.setAttribute("ratingList",ratingList);
		
		
		request.getRequestDispatcher("WEB-INF/views/news.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
