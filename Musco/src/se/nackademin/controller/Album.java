package se.nackademin.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.AlbumManager;
import se.nackademin.ejb.RatingManager;
import se.nackademin.ejb.ReviewManager;
import se.nackademin.model.AlbumGenre;
import se.nackademin.model.Rating;
import se.nackademin.model.User;

@WebServlet("/Album")
public class Album extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Inject
	AlbumManager am;
	@Inject
	RatingManager rm;
	@Inject
	ReviewManager revm;
	
	User user;
	Date date;
	
    public Album() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		user = (User) session.getAttribute("member");
		String albumId = request.getParameter("byid");
		
		se.nackademin.model.Album album = am.findAlbumById(albumId);
		List<Rating> ratings = rm.findRatingsByAlbum(album.getId());
		List<se.nackademin.model.Review> reviews = revm.getReviewsByAlbum(albumId);
		AlbumGenre firstGenre;
		try{
			firstGenre = album.getAlbumGenres().get(0);
		}catch(ArrayIndexOutOfBoundsException e){
			firstGenre = null;
		}
		
		if(firstGenre != null){
			List<se.nackademin.model.Album> albumsByGenre = am.findAlbumsByGenre(firstGenre.getGenreBean().getName(), 6);
			if(albumsByGenre != null){
				request.setAttribute("albumsByGenre", albumsByGenre);
			}
		}
		
		if(ratings != null){
			request.setAttribute("ratings", ratings);
		}
		if(reviews != null){
			request.setAttribute("reviews", reviews);
		}

		
		if(session.getAttribute("member") != null){
			Rating rating = rm.findRatingByAlbumAndUser(albumId, user.getName());
			if(rating != null){
				request.setAttribute("rating_given", rating);
			}else{
				
			}
		}
		
		if(album != null){
			request.setAttribute("album", album);
			request.getRequestDispatcher("WEB-INF/views/album.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("WEB-INF/views/artists.jsp").forward(request, response);
		}
		

		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		
		if(session.getAttribute("member") != null){
			String albumId = request.getParameter("albumId");
			user = (User) session.getAttribute("member");
			String userRating = request.getParameter("fader");
			date = new Date();
			
			Rating rating = new Rating();
			int parsedRating = Integer.parseInt(userRating);
			rating.setRating(parsedRating);
			rating.setDate(date);
			rating.setUserBean(user);
			rating.setAlbumBean(am.findAlbumById(albumId));
			rm.saveRating(rating);
			response.sendRedirect("Album?byid=" + albumId);
		}

	}

}
