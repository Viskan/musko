package se.nackademin.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.AlbumManager;
import se.nackademin.ejb.ArtistManager;
import se.nackademin.ejb.FavBandManager;
import se.nackademin.model.Album;
import se.nackademin.model.ArtistGenre;
import se.nackademin.model.FavBand;
import se.nackademin.model.User;
import se.nackademin.ejb.RatingManager;



@WebServlet("/Artist")
public class Artist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	ArtistManager am;
	@Inject
	AlbumManager alm; 
	@Inject
	FavBandManager fm;  
	@Inject
	RatingManager rm;

	public Artist() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String artistId = request.getParameter("byid");
		ArtistGenre firstGenre;

		if(artistId == null){
			response.sendRedirect("Artists");
		}else{
			se.nackademin.model.Artist artist = am.getArtistById(artistId);
			request.setAttribute("artist", artist);

			try{
				firstGenre =  artist.getArtistGenres().get(0);
			}catch(ArrayIndexOutOfBoundsException e){
				firstGenre = null;
			}

			if(firstGenre != null){
				List<se.nackademin.model.Artist> artistsByGenre = am.getArtistByGenre(firstGenre.getGenreBean().getName(), 5);
				if(artistsByGenre != null){
					request.setAttribute("artistsByGenre", artistsByGenre);
				}
			}

			List<Album> albums = alm.findAlbumsByArtist(artistId);
			request.setAttribute("albums", albums);

			List<FavBand> favBands = fm.getFavBandsByArtist(artistId);
			request.setAttribute("favBands", favBands);

			request.getRequestDispatcher("WEB-INF/views/artist.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if(session.getAttribute("member") != null){
			String whatDoTo = request.getParameter("addOrDelete");

			String favBandArtist = request.getParameter("favArtist");
			int artistId = Integer.parseInt(favBandArtist);
			se.nackademin.model.Artist a = new se.nackademin.model.Artist();
			a.setId(artistId);
			User u = (User) session.getAttribute("member");
			FavBand favBand = new FavBand();
			favBand.setArtistBean(a);
			favBand.setUserBean(u);


			if(whatDoTo.equalsIgnoreCase("add")){

				fm.saveFavBand(favBand);
				response.sendRedirect("Artist?byid="+artistId);

			}else{

				favBand = fm.getFavband(favBand);
				fm.deleteFavBand(favBand);
				response.sendRedirect("Artist?byid="+artistId);
			}
		}
	}

}
