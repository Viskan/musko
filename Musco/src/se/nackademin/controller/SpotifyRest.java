package se.nackademin.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import se.nackademin.ejb.AlbumManager;
import se.nackademin.ejb.ArtistManager;
import se.nackademin.model.Album;
import se.nackademin.model.AlbumGenre;
import se.nackademin.model.Albumtype;
import se.nackademin.model.Artist;
import se.nackademin.model.ArtistGenre;
import se.nackademin.model.Genre;;


@Path("spotifyRest")
public class SpotifyRest {
	@EJB
	ArtistManager am;
	@EJB
	AlbumManager alm;

	@GET
	public void lala(){
		System.out.println("lala");
	}

	@GET
	@Path("isNew/{id}")
	@Produces("application/json")
	public Artist isNew(@PathParam("id") String id){
		Artist a = am.findArtistBySpotifyID(id);

		return a;

	}
	@GET
	@Path("isNewAlbum/{id}")
	@Produces("application/json")
	public Album isNewAlbum(@PathParam("id") String id){
		Album a = alm.findAlbumBySpotifyID(id);
		return a;

	}






	@POST
	@Path("addArtist")
	public void addArtist(@FormParam("artistModalName")String name, @FormParam("artistModalSpotifyId")String spotify, 
			@FormParam("artistModalImageUrl")String url, @FormParam("artistModalId") int id,  @FormParam("artistModalBio") String bio,
			@FormParam("genres") List<String> genres){
		if(spotify==null || spotify == ""){
			return;

		}
		Artist a = new Artist();
		if(!(id==0)){

			a.setId(id);

		}else{


		}
		a.setSpotifyId(spotify);
		a.setName(name);
		a.setImage(url);
		a.setDescription(bio);

		if(genres!=null){
			List<ArtistGenre> artistGenres = new ArrayList<ArtistGenre>();
			for(String g : genres){
				Genre gen = new Genre();
				gen.setName(g);
				ArtistGenre ag = new ArtistGenre();
				ag.setGenreBean(gen);
				ag.setArtistBean(a);
				artistGenres.add(ag);
			}
			a.setArtistGenres(artistGenres);

		}
		am.saveArtist(a);
		//		System.out.println(name);
		//				System.out.println(spotify);
		//		System.out.println(url);
		//		System.out.println(id);
		//		System.out.println(bio);
	}
	@POST
	@Path("addAlbum")
	public void addAlbum(@FormParam("albumModalName")String name, @FormParam("albumModalSpotifyId")String spotify, 
			@FormParam("albumModalImageUrl")String url, @FormParam("albumModalId") int id,  @FormParam("albumModalReleaseDate") String StringDate,
			@FormParam("albumModalAlbumType") String type,@FormParam("albumModalArtistSpotifyId") String artistSpotify,@FormParam("genres") List<String> genres){
//		System.out.println(name);
//		System.out.println(spotify);
//		System.out.println(url);
//		System.out.println(id);
//		System.out.println(StringDate);
//		System.out.println(type);
		Album a = new Album();
		if(!(id==0)){

			a.setId(id);

		}
		a.setName(name);
		a.setSpotifyId(spotify);
		a.setImage(url);
		Albumtype at = new Albumtype();
		at.setName(type);
		a.setAlbumtype(at);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date date = null;
		try {
			date = format.parse(StringDate);
			
		} catch (ParseException e) {
//			date = format.parse("0000-00-00");
		}
		a.setReleaseDate(date);
		
		if(genres!=null){
			List<AlbumGenre> albumGenres = new ArrayList<AlbumGenre>();
			for(String g : genres){
				Genre gen = new Genre();
				gen.setName(g);
				AlbumGenre ag = new AlbumGenre();
				ag.setGenreBean(gen);
				ag.setAlbumBean(a);
				albumGenres.add(ag);
			}
			a.setAlbumGenres(albumGenres);

		}
		
		Artist artist = am.findArtistBySpotifyID(artistSpotify);
		if(artist!=null){
			a.setArtistBean(artist);
//			System.out.println(artistSpotify);
			alm.saveAlbum(a);
		}
		
		//a.se
	}

}