package se.nackademin.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.userManager;
import se.nackademin.model.User;
import se.nackademin.utility.SHA256;

@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	userManager um;
	RequestDispatcher view = null; 
       
    public Register() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		view = request.getRequestDispatcher("WEB-INF/views/register.jsp");
		view.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String username = request.getParameter("username");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String profileImg = request.getParameter("profile_img_src");
		String mail = request.getParameter("Email");
		
		if(profileImg == null){
			profileImg = "http://upload.wikimedia.org/wikipedia/commons/3/37/No_person.jpg";
		}
		
		if(!password1.equals(password2)){
			request.setAttribute("password_error", true);
			request.setAttribute("error", true);
		}else{
			if(password1.length() < 6){
				request.setAttribute("pass_length_error", true);
				request.setAttribute("error", true);
			}
		}
		
		if(!um.validateUsername(username)){
			request.setAttribute("username_taken_error", true);
			request.setAttribute("error", true);
		}else if(username.length() < 2 || username.length() > 20){
			request.setAttribute("username_length_error", true);
			request.setAttribute("error", true);
		}else{
			request.setAttribute("username", username);
		}
		
		
		if(request.getAttribute("error") != null){
			view = request.getRequestDispatcher("WEB-INF/views/register.jsp");
			view.forward(request, response);
		}else{
			SHA256 hash = new SHA256(password1);
			password1 = hash.getHashValue();
			User user = new User();
			user.setName(username);
			user.setPassword(password1);
			user.setMail(mail);
			user.setImage(profileImg);
			user.setUserLevel(0);
			um.registerUser(user);

			HttpSession session = request.getSession();
			session.setAttribute("member", user);

			response.sendRedirect("Profile");
			
		}
	}

}
