package se.nackademin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.GenreManager;
import se.nackademin.model.ArtistGenre;

@WebServlet("/Genre")
public class Genre extends HttpServlet {

	@Inject 
	GenreManager gm;

	private static final long serialVersionUID = 1L;

	public Genre() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String genreLetter = "";

		if(request.getParameter("letter") == null){
			genreLetter = "a";
		}else{
			genreLetter = request.getParameter("letter");
		}
		if(genreLetter.equals("sign")){
			genreLetter = "2";
		}
		List<se.nackademin.model.Genre> genres = gm.getAllGenres(genreLetter);
		request.setAttribute("genres", genres);
		
		if(request.getParameter("genre") != null){
			List<ArtistGenre> artists = gm.getArtistsByGenre(request.getParameter("genre"));
			request.setAttribute("artistByGenre", artists);
			
			if(artists.isEmpty()){
				System.out.println("Ingen artist i denna genre");
				request.setAttribute("emptyGenre", "No artist tagged in this genre.");
			}
		}

		request.getRequestDispatcher("WEB-INF/views/genres.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
