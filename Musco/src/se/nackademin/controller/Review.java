package se.nackademin.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.CommentManager;
import se.nackademin.ejb.ReviewManager;
import se.nackademin.model.Comment;
import se.nackademin.model.User;

@WebServlet("/Review")
public class Review extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Inject
	ReviewManager rm;
	@Inject
	CommentManager cm;
	
    public Review() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
	
		String reviewId = request.getParameter("byid");
		if(reviewId == null){
			response.sendRedirect("Reviews");
		}else{
		se.nackademin.model.Review review = rm.getReviewById(reviewId);
		request.setAttribute("review", review);
		List<Comment> comments = cm.getCommentsByReview(reviewId);
		request.setAttribute("comments", comments);
		
		
		
		request.getRequestDispatcher("WEB-INF/views/review.jsp").forward(request, response);
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if(session.getAttribute("member") != null){
			User u = (User) session.getAttribute("member");
			String comment = request.getParameter("commentText");
			String onReview = request.getParameter("onReview");
			Date now = new Date();
			
			Comment c = new Comment();
			c.setDate(now);
			c.setReviewBean(rm.getReviewById(onReview));
			c.setText(comment);
			c.setUserBean(u);
			cm.saveComment(c);
			response.sendRedirect("Review?byid="+onReview);
			
		}
	}

}
