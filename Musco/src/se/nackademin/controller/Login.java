package se.nackademin.controller;

import java.io.IOException;


import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.userManager;
import se.nackademin.model.User;

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private userManager um;

	public Login() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		User user = new User();
		user.setName(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		
		user = um.findUser(user);
		
		RequestDispatcher view;
		
		HttpSession session = request.getSession();
		
		if(user != null){
			session.setAttribute("member", user);
			response.sendRedirect("MainPage");
		}
		else{
			session.invalidate();
			request.setAttribute("loginerror", true);
			response.sendRedirect("https://localhost:8181/Musco/MainPage?login=error");
		}
	}

}
