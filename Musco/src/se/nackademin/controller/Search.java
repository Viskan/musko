package se.nackademin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.nackademin.ejb.ArtistManager;
import se.nackademin.model.Artist;

import com.google.gson.Gson;

@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Inject
	ArtistManager am; 
	
    public Search() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String search = request.getParameter("search_artist").replaceAll("[^a-zA-Z\\s]+", "");
		Artist artist = am.findArtistByExactMatch(search);
		List<Artist> list = am.getArtistsBySearch(search);
		
		if(artist != null){
			response.sendRedirect("/Musco/Artist?byid=" + artist.getId());
		}else if(!list.isEmpty()){
			request.setAttribute("search_list", list);
			
			List<se.nackademin.model.Artist> artistList = am.getAllArtists();
			request.setAttribute("artistList", artistList);
			
			request.getRequestDispatcher("WEB-INF/views/search_results.jsp").forward(request, response);
			request.setAttribute("artistList", artistList);
		}else{
			request.setAttribute("search_error", true);
			request.getRequestDispatcher("WEB-INF/views/search_results.jsp").forward(request, response);
		}
		
	}

}
