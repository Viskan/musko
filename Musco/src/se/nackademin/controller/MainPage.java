package se.nackademin.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.nackademin.ejb.CommentManager;
import se.nackademin.ejb.ReviewManager;
import se.nackademin.model.Comment;


@WebServlet("/MainPage")
public class MainPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Inject
	ReviewManager rm;
	@Inject
	CommentManager cm;

    public MainPage() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<se.nackademin.model.Review> reviewList = rm.getReviewSortByDate(3);
		List<Comment> commentList = cm.getLatestComments(6);
		String login = request.getParameter("login");
		
		if(login != null){
			request.setAttribute("loginerror", true);
		}
		
		if(reviewList != null){
			request.setAttribute("reviewList", reviewList);
		}
		if(commentList != null){
			request.setAttribute("commentList", commentList);
		}
		
		request.setCharacterEncoding("UTF-8");
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/views/index.jsp");
		view.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
