package se.nackademin.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.AlbumManager;
import se.nackademin.model.Albumtype;
import se.nackademin.model.User;

/**
 * Servlet implementation class Spotify
 */
@WebServlet("/Spotify")
public class Spotify extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
	AlbumManager am;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Spotify() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("member") != null){
			User user = (User) session.getAttribute("member");
			if(user.getUserLevel() > 0){
				request.setCharacterEncoding("UTF-8");
				List<Albumtype> albumTypes = am.getAllAlbumTypes();
				request.setAttribute("albumTypes", albumTypes);
				RequestDispatcher view = request.getRequestDispatcher("WEB-INF/views/spotify.jsp");
				view.forward(request, response);
			}else{
				response.sendRedirect("MainPage");
			}
		}else{
			response.sendRedirect("MainPage");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
