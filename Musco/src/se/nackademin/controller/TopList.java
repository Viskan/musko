package se.nackademin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.nackademin.ejb.TopListManager;
import se.nackademin.model.FavBand;

@WebServlet("/TopList")
public class TopList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Inject
	TopListManager tlm;
       
    public TopList() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//String favBandId = request.getParameter("byid");
		
		//List<FavBand> favb = tlm.getFavBands(20);
		List<FavBand> favBands = tlm.getFavBands();
		
		  Map<Integer, Integer> bandCount = new HashMap<Integer,Integer>();
		    for(FavBand band: favBands) {
		       Integer key = band.getArtistBean().getId();
		       Integer value = bandCount.get(key);
		       if(value == null){
		    	   value = 0;
		       }
		       
		       bandCount.put(key, value + 1);
		    }
		    
		    List<Integer> sorted = new ArrayList<Integer>(bandCount.values());
		   
		   
		    
		    
		
		request.setAttribute("listFavBands", sorted);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/views/toplist.jsp");
		view.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
