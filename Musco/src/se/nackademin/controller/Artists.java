package se.nackademin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import se.nackademin.ejb.ArtistManager;

@WebServlet("/Artists")
public class Artists extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Inject
	ArtistManager am;
       
    public Artists() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		List<se.nackademin.model.Artist> artistList = am.getAllRecentArtists(12);
		request.setAttribute("artistList", artistList);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/views/artists.jsp");
		view.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

}
