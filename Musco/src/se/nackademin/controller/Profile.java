package se.nackademin.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import se.nackademin.ejb.CommentManager;
import se.nackademin.ejb.FavBandManager;
import se.nackademin.ejb.RatingManager;
import se.nackademin.ejb.ReviewManager;
import se.nackademin.ejb.userManager;
import se.nackademin.model.Comment;
import se.nackademin.model.FavBand;
import se.nackademin.model.User;
import se.nackademin.model.Review;
import se.nackademin.model.Rating;

@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	ReviewManager rm;
	@Inject
	CommentManager cm;
	@Inject
	FavBandManager fm;
	@Inject
	RatingManager rtm;
	@Inject
	userManager um;


	public Profile() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if(session.getAttribute("member") != null){
			String profile = request.getParameter("profile");
			if(profile != null){
				List<Review> reviews = rm.getReviewsByUser(profile);
				request.setAttribute("reviews", reviews);
				List<Comment> comments = cm.getCommentsByUser(profile);
				request.setAttribute("comments", comments);
				User thisProfile  = um.getUser(profile);
				request.setAttribute("thisProfile",thisProfile);
				List<FavBand> favBands = fm.getFavBandsByUser(profile);
				request.setAttribute("favBands", favBands);
				List<Rating> ratings = rtm.getAllRatingsByUser(profile);
				request.setAttribute("ratings", ratings);
				
			}else{
				User user = (User) session.getAttribute("member");
				List<Review> reviews = rm.getReviewsByUser(user.getName());
				request.setAttribute("reviews", reviews);
				List<Comment> comments = cm.getCommentsByUser(user.getName());
				request.setAttribute("comments", comments);
				User thisProfile  = um.getUser(user.getName());
				request.setAttribute("thisProfile",thisProfile);
				List<FavBand> favBands = fm.getFavBandsByUser(user.getName());
				request.setAttribute("favBands", favBands);
				List<Rating> ratings = rtm.getAllRatingsByUser(user.getName());
				request.setAttribute("ratings", ratings);
			}
			request.getRequestDispatcher("WEB-INF/views/profile.jsp").forward(request, response);
		}else{
			request.setAttribute("access_error", true);
			request.getRequestDispatcher("WEB-INF/views/register.jsp").forward(request, response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
