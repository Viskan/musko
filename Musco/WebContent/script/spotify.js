$('#searchArtist').on('input propertychange paste', function() {
	var input = $("#searchArtist").val();
	var replacedInput = input.replace(/ /g, '+');
	$.getJSON("https://api.spotify.com/v1/search?q=" + replacedInput +"&type=artist", function(result){
		if(result.artists.items.length>0){
			$("#artistList").empty();
			var answerSize = result.artists.items.length;
			for (var i=0;i<answerSize;i++) { 
				$("#artistList").append('<li class="list-group-item" data-spotify-link="' + result.artists.items[i].id + '" onclick="viewArtist(this)"><img src="'+ result.artists.items[i].images[0].url +'" class="img-thumbnail artistListPic" alt="No pic!">' + result.artists.items[i].name + '</li>');
			}
		}
	});
});

function viewArtist(choice){
	$(window).scrollTop($('#viewArtist').offset().top);
	var artist = $(choice).attr("data-spotify-link");


	$.getJSON("https://api.spotify.com/v1/artists/"+artist, function(result){
		$("#viewArtistName").html(result.name);
		$("#viewArtistPic").attr("src", result.images[0].url);
		$("#viewArtistSpotifyID").html(result.id);
		$("#viewArtistSpotifyFollowers").html("Followers: "+result.followers.total);
		$("#viewArtistGenres").empty();
		for(var i =0; i<result.genres.length;i++){
			$("#viewArtistGenres").append('<li>'+result.genres[i]+'</li>');
		}
	});
};

function viewAlbums(){
	var artist =  $("#viewArtistSpotifyID").html();
	$.getJSON("https://api.spotify.com/v1/artists/"+artist+"/albums?album_type=album,compilation&market=SE&limit=50", function(result){
		$("#viewAlbums").empty();
		$("#viewAlbumsTotal").html("Albums: " + result.total);
		if(result.items.length>0){
			var answerSize = result.items.length;
			var bild;
			for (var i=0;i<answerSize;i++) {

				if(result.items[i].images[0]!=null){
					bild = result.items[i].images[0].url;
				}else{
					bild="https://www.dinafem.org/static/images/site/no-photo.jpg";
				}
				
				$("#viewAlbums").append('<div class="col-md-2 albumThumbs"><div class="thumbnail">'
						+'<img src="' + bild + '" alt="No pic" class="img-responsive">'
						+ '<div class="caption"><h4>'+ result.items[i].name +'</h4>'
						+'<p>'+ result.items[i].album_type +'</p>'
						+'<p><a onclick="addEditAlbum(this)" class="btn btn-success btn-xs" role="button" data-toggle="modal" data-target="#albumModal" data-artist-spotify="'+ artist +'" data-spotify-link="' + result.items[i].id + '">Add / Edit</a></p></div></div></div>');

			}if(result.next != null){
				viewMoreAlbums(result.next, artist);				
			}
			$(window).scrollTop($('#viewAlbums').offset().top);
		}
	});
};

function viewMoreAlbums(link,artist){
	$.getJSON(link,function(result){
		if(result.items.length>0){
			var answerSize = result.items.length;
			var bild;
			for (var i=0;i<answerSize;i++) {

				if(result.items[i].images[0]!=null){
					bild = result.items[i].images[0].url;
				}else{
					bild="https://www.dinafem.org/static/images/site/no-photo.jpg";
				}
				$("#viewAlbums").append('<div class="col-md-2 albumThumbs"><div class="thumbnail">'
						+'<img src="' + result.items[i].images[0].url + '" alt="No pic" class="img-responsive">'
						+ '<div class="caption"><h4>'+ result.items[i].name +'</h4>'
						+'<p>'+ result.items[i].album_type +'</p>'
						+'<p><a onclick="addEditAlbum(this)" class="btn btn-success btn-xs" role="button" data-toggle="modal" data-target="#albumModal" data-artist-spotify="'+ artist +'" data-spotify-link="' + result.items[i].id + '">Add / Edit</a></p></div></div></div>');

			}if(result.next != null){
				viewMoreAlbums(result.next);
			}

		}

	});

};


function addEditArtist(){
	$("#artistModalForm")[0].reset();
	$("#artistModalSpotifyIdHidden").val("");
	$("#artistModalIdHidden").val("");
	$.getJSON('http://localhost:8080/Musco/resources/spotifyRest/isNew/' + $("#viewArtistSpotifyID").html(), function(muskoResult){
//		$.getJSON("/Musco/SpotifyAddEdit?spotifyID="+ $("#viewArtistSpotifyID").html(), function(muskoResult){
		if(!jQuery.isEmptyObject(muskoResult)){
			$("#artistModalName").val(muskoResult.name);
			$("#artistModalImageUrl").val(muskoResult.image);
			$("#artistModalImage").attr("src", muskoResult.image);
			$("#artistModalId").val(muskoResult.id);
			$("#artistModalIdHidden").val(muskoResult.id);
			$("#artistModalSpotifyId").val(muskoResult.spotifyId);
			$("#artistModalSpotifyIdHidden").val(muskoResult.spotifyId);
			$("#artistModalBio").val(muskoResult.description);
			$("#artistModalGenres").empty();


		}else{//alert("ny artist");
			$("#artistModalName").val($("#viewArtistName").html());
			$("#artistModalSpotifyId").val($("#viewArtistSpotifyID").html());
			$("#artistModalSpotifyIdHidden").val($("#viewArtistSpotifyID").html());
			$("#artistModalImageUrl").val($("#viewArtistPic").attr("src"));
			$("#artistModalImage").attr("src",$("#artistModalImageUrl").val());
			$("#artistModalGenres").empty();
			$.each($("#viewArtistGenres").children(), function(){
				$("#artistModalGenres").append('<label class="genresCheck"><input type="checkbox" name="genres" value="'+ $(this).text() +'" checked>'+ $(this).text() +'</label>');
			});

		} 
	});


}

$("#artistModalImageUrl").change(function(){
	$("#artistModalImage").attr("src",$("#artistModalImageUrl").val());
});
$("#albumModalImageUrl").change(function(){
	$("#albumModalImage").attr("src",$("#albumModalImageUrl").val());
});




function addGenres(){
	$("#artistModalGenres").append('<label class="genresCheck"><input type="checkbox" name="genres" value="'+ $("#artistModalGenresList").val() +'" checked>'+ $("#artistModalGenresList").val() +'</label>')};


	function addArtist(){
		$.post("http://localhost:8080/Musco/resources/spotifyRest/addArtist", function(result){

		});
	}	

	function addEditAlbum(choice){
		$("#albumModalForm")[0].reset();
		$("#albumModalSpotifyIdHidden").val("");
		$("#albumModalIdHidden").val("");
		$("#albumModalGenres").empty();
		$("#albumModalArtistSpotifyId").val($(choice).attr("data-artist-spotify"));
		var album = $(choice).attr("data-spotify-link");
		$.getJSON('http://localhost:8080/Musco/resources/spotifyRest/isNewAlbum/' + album, function(muskoResult){
			if(!jQuery.isEmptyObject(muskoResult)){
				$("#albumModalName").val(muskoResult.name);
				$("#albumModalSpotifyId").val(muskoResult.spotifyId);
				$("#albumModalSpotifyIdHidden").val(muskoResult.spotifyId);
				$("#albumModalImageUrl").val(muskoResult.image);
				$("#albumModalImage").attr("src",$("#albumModalImageUrl").val());
				$("#albumModalReleaseDate").val(muskoResult.releaseDate);
				$("#albumModalIdHidden").val(muskoResult.id);
				$("#albumModalId").val(muskoResult.id);

			}else{
				addEditNewAlbum(album);
			}
		});
	}
		
function addEditNewAlbum(album){
	$.getJSON('https://api.spotify.com/v1/albums/' + album, function(result){
		$("#albumModalName").val(result.name);
		$("#albumModalSpotifyId").val(result.id);
		$("#albumModalSpotifyIdHidden").val(result.id);
		$("#albumModalImageUrl").val(result.images[0].url);
		$("#albumModalImage").attr("src",$("#albumModalImageUrl").val());
		$("#albumModalReleaseDate").val(result.release_date);
		for(var i =0; i<result.genres.length;i++){
			$("#albumModalGenres").append('<label class="genresCheck"><input type="checkbox" name="genres" value="'+ result.genres[i] +'" checked>'+ result.genres[i] +'</label>');
		}
	});
}
function addAlbumGenres(){
	$("#albumModalGenres").append('<label class="genresCheck"><input type="checkbox" name="genres" value="'+ $("#albumModalGenresList").val() +'" checked>'+ $("#albumModalGenresList").val() +'</label>')};
