var clicked = false;


$(document).ready(function() {  
	$("#more-button").click(function() {

		if(!clicked){

			$("#artist-description").css("overflow", "visible");
			$("#artist-description").css("max-height", "none");
			$("#more-button").text("less..");
			clicked = true;

		}else{
			$("#artist-description").css("overflow", "hidden");
			$("#artist-description").css("max-height", 200);
			$("#more-button").text("more..");
			clicked = false;
		}

	});
	
	$("#rate-button").click(function(){
		
		var rating = $("#volume").val;
	});
	
	$("#preview-button").click(function(){
		var img = $("#profile_img_src").val();
		$("#profile_img").attr("src", img);
	});
});

function outputUpdate(vol) {
	$('#rating').val(vol);

	}

function playAlbum(id){
	$("#spotifyPlayer").empty();
	var spotify =  $(id).attr("data-spotify");
	$("#spotifyPlayer").html('<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A'+ spotify +'" width="230" height="330" frameborder="0" allowtransparency="true"></iframe>');
};

function addDeleteFav(){
	$("#favBandsFrom").submit();
}
