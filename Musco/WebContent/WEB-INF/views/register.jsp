<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<%@ page import="se.nackademin.model.User"%>
<%
	User user = (User) session.getAttribute("member");
%>
</head>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<!-- <form method="post" action="Register">
								<button type="submit" class="btn btn-warning">
								<span class="glyphicon glyphicon-user" aria-hidden ="true"
										aria-label="Register"><small>+</small></span>
								</button>
								</form>  -->
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<%
			if (request.getAttribute("error") != null) {
		%>
		<div class="alert alert-danger" role="alert">
			<ul>
				<%
					if (request.getAttribute("username_taken_error") != null) {
				%>
				<li><span class="glyphicon glyphicon-exclamation-sign"
					aria-hidden="true"></span> <span class="sr-only">Error:</span>
					Username is taken.</li>
				<%
					}
				%>
				<%
					if (request.getAttribute("username_length_error") != null) {
				%>
				<li><span class="glyphicon glyphicon-exclamation-sign"
					aria-hidden="true"></span> <span class="sr-only">Error:</span>
					Username is too short.</li>
				<%
					}
				%>
				<%
					if (request.getAttribute("pass_length_error") != null) {
				%>
				<li><span class="glyphicon glyphicon-exclamation-sign"
					aria-hidden="true"></span> <span class="sr-only">Error:</span>
					Password is too short.</li>
				<%
					}
				%>
				<%
					if (request.getAttribute("password_error") != null) {
				%>
				<li><span class="glyphicon glyphicon-exclamation-sign"
					aria-hidden="true"></span> <span class="sr-only">Error:</span>
					Passwords do not match.</li>
				<%
					}
				%>
			</ul>
		</div>
		<%
			}
		%>
		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">

			<div class="row">
				<div class="col-md-6" align="center">
					<form class="form-signin col-sm-6 col-md-6" method="post"
						action="Register" style="margin-left: 200px;">
						<h2 class="form-signin-heading">Register User</h2>
						<label for="" class="sr-only">Email address</label> <input
							type="text" id="" class="form-control" placeholder="Username"
							name="username" required autofocus> <label
							for="inputPassword" class="sr-only">Password</label> <input
							type="password" id="" class="form-control" placeholder="Password"
							name="password1" required> <label for="inputPassword"
							class="sr-only">Password</label> <input type="password" id=""
							class="form-control" placeholder="Repeat Password"
							name="password2" required> <label for="inputEmail"
							class="sr-only">Password</label> <input type="text"
							id="inputEmail" class="form-control" placeholder="Email"
							name="Email" required> <input type="text"
							class="form-control" placeholder="Insert picture URL"
							id="profile_img_src" name="profile_img_src"><br>

						<button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
					</form>
				</div>
				<div class="col-md-0"></div>
				<div class="col-md-2" style="margin-top: 45px;">
					<div class="thumbnail">
						<img
							src="http://upload.wikimedia.org/wikipedia/commons/3/37/No_person.jpg"
							id="profile_img">
					</div>
					<button class="btn btn-lg btn-primary btn-block"
						id="preview-button">Preview image</button>
				</div>
				<%
					if (request.getAttribute("access_error") != null) {
				%>
				<div class="col-md-6">
					<span class="label label-danger">Please log in or register
						to access this page.</span>
				</div>
				<%
					}
				%>
			</div>
			<!-- /container -->

		</div>


		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>
	<script src="./script/main.js"></script>
</body>
</html>