<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/reviews.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Review"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%@ page import="se.nackademin.model.User"%>
<%User user = (User)session.getAttribute("member"); %>
</head>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>
		<br>

		<!-- Main component for a primary marketing message or call to action -->
		<% Calendar cal = Calendar.getInstance(); %>
		<div class="row">
			<div class="col-md-9">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Most recent reviews</h3>
					</div>
					<div class="panel-body">
						<!-- thumbnail row -->
						<div class="row">
							<div class="col-md-12">
								<%
							List<Review>reviewsByDate = (List<Review>) request.getAttribute("reviewsSortedByDate");
						%>
								<%
							if (reviewsByDate != null) {
						%>

								<%
							for (Review rd : reviewsByDate) {
								cal.setTime(rd.getDate());
								SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
								String formatted = format1.format(cal.getTime());
								
						%>

								<div class="col-md-3 col-sm-3 albumThumbs">
								
									<a href="Review?byid=<%=rd.getId()%>" class="thumbnail"> <img
										src="<%=rd.getAlbumBean().getImage()%>" alt="...">
										<div class="caption">
											<h5>
												<strong><%=rd.getAlbumBean().getArtistBean().getName()%></strong>
											</h5>
											<h5>
												<strong><%=rd.getAlbumBean().getName()%></strong>
											</h5>
											
											<div class="userDate">
												<small>by: <%=rd.getUserBean().getName()%></small><br>
												<small>added: <%=formatted %></small>

											</div>


										</div>

									</a>

								</div>


								<%
								}
							%>
								<%
								}
							%>
							</div>
						</div>

						<!-- /thumbnail row -->

					</div>
				</div>
			</div>

			<div class="col-md-3 reviewList">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Reviews</h3>
					</div>
					<ul class="list-group sidelist">
						
							<%
							List<Review> reviews = (List<Review>) request.getAttribute("allReviews");
						%> <%
							if (reviews != null) {
						%> <%
							for (Review r : reviews) {
						%> <a href="Review?byid=<%=r.getId()%>" class="list-group-item">
								<%=r.getAlbumBean().getArtistBean().getName()%> - <%=r.getAlbumBean().getName()%> 
								<br>
								<span id="byuser">by: <%=r.getUserBean().getName()%></span>
								<%
								}
							%>
						</a>
						
						 <%
							}
						%>
						
					</ul>
				</div>

			</div>


		</div>
		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>

</body>
</html>