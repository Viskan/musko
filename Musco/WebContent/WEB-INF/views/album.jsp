<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/artist.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Genre"%>
<%@ page import="se.nackademin.model.AlbumGenre"%>
<%@ page import="se.nackademin.model.Artist"%>
<%@ page import="se.nackademin.model.Album"%>
<%@ page import="se.nackademin.model.Rating"%>
<%@ page import="se.nackademin.model.User"%>
<%@ page import="se.nackademin.model.Review"%>
<%@ page import="se.nackademin.model.AlbumGenre"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.util.Comparator"%>
<%@ page import="java.util.Collections"%>
<%User user = (User)session.getAttribute("member"); %>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<!-- Main component for a primary marketing message or call to action -->
		<% se.nackademin.model.Album album = (Album)request.getAttribute("album"); %>
		<%List<AlbumGenre> genreList= album.getAlbumGenres(); 
		  List<Album> albumsByID = (List<Album>)request.getAttribute("albumsByGenre");%>
		<%
			Calendar cal = Calendar.getInstance();
		%>
		<div class="row">
			<div class="col-md-3">
				<div class="thumbnail">
					<img src="<%=album.getImage() %>" alt="">
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Similar Albums</h3>
							</div>
							<%if (albumsByID != null){%>
							
								<ul class="list-group">
							 	<% for(Album a: albumsByID){%>
							 	<%if(album.getName().equals(a.getName())){continue;} %>
								<a href="Album?byid=<%=a.getId()%>"  class="list-group-item"><strong><%=a.getName()%></strong> - <%=a.getArtistBean().getName() %></a>
								
							<%	}%>
								</ul>
							<% }%>	
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Spotify Player</h3>
								
							</div>
							<div class="panel-body">
								<iframe
									src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A<%=album.getSpotifyId() %>"
									width="230" height="330" frameborder="0"
									allowtransparency="true"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="panel panel-success" id="albumPanel">
					<div class="panel-heading">
						<h1 class="panel-title">
							<a href="Artist?byid=<%= album.getArtistBean().getId()%>"><strong><%=album.getName() + " - " + album.getArtistBean().getName() %></strong></a>
						</h1>
					</div>
					<div class="panel-body">
						<%for(AlbumGenre a: genreList){ %>
						<span class="label label-info"><a
							href="Genre?genre=<%=a.getGenreBean().getName() %>&letter=<%=a.getGenreBean().getName().substring(0, 1) %>"><%=a.getGenreBean().getName() %></a></span>
						<%} %>
						<% List<Rating> ratings = (List<Rating>)request.getAttribute("ratings");
								int ratingSum = 0;
								int avgRating = 0;
								for(Rating r: ratings){ 
									ratingSum += r.getRating();
								}
								if(ratings.isEmpty()){avgRating = 0;}else{avgRating = ratingSum/ratings.size();}
								
								%>
						<h3>
							Average Album Rating: <span id="avg-rating"><%=avgRating %></span>
						</h3>
						<br>
						<div id="artist-description">
							<form method="post" action="Album">
								<%Rating ratingGiven = (Rating)request.getAttribute("rating_given"); %>
								<%if(user != null){ %>
								<%if(ratingGiven != null){ %>
								<label for=fader>Rate the album</label> <input type="range"
									min="1" max="100" value="<%=ratingGiven.getRating() %>"
									id="fader" name="fader" step="1" oninput="outputUpdate(value)"
									disabled> <input type="hidden"
									value="<%=album.getId() %>" name="albumId">
								<div class="row">
									<div class="col-md-5"></div>
									<div class="col-md-7">
										<output for="fader" id="rating"><%=ratingGiven.getRating() %></output>
										<span id="max-rating">/100</span><br>
									</div>
								</div>
								<%}else{ %>
								<label for=fader>Rate the album</label> <input type="range"
									min="1" max="100" value="50" id="fader" name="fader" step="1"
									oninput="outputUpdate(value)"> <input type="hidden"
									value="<%=album.getId() %>" name="albumId">
								<div class="row">
									<div class="col-md-5"></div>
									<div class="col-md-7">
										<output for="fader" id="rating">50</output>
										<span id="max-rating">/100</span><br>
										<button type="submit" class="btn btn-info btn-md"
											id="rate-button">Add Rating</button>
									</div>
								</div>
								<%} %>
								<%}else{ %>
								<h3>Log in to rate album.</h3>

								<%} %>
							</form>

						</div>
						<div class="row">
							<div class="col-md-11"></div>
							<div class="col-md-1"></div>

						</div>

					</div>

				</div>
				<div class="row">

					<div class="col-md-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Reviews</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="row">
										<div class="col-md-12">
											<%
							List<Review>reviewsByAlbum = (List<Review>) request.getAttribute("reviews");
						%>
											<%
							if (reviewsByAlbum != null) {
						%>

											<%
							for (Review rd : reviewsByAlbum) {
								cal.setTime(rd.getDate());
								SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
								String formatted = format1.format(cal.getTime());
								
						%>

											<div class="col-md-3 col-sm-3 albumThumbs">

												<a href="Review?byid=<%=rd.getId()%>" class="thumbnail">
													<img src="<%=rd.getUserBean().getImage()%>" alt="..." id="review-image">
													<div class="caption">
														<h5>
															<strong><%=rd.getAlbumBean().getArtistBean().getName()%></strong>
														</h5>
														<h5>
															<strong><%=rd.getAlbumBean().getName()%></strong>
														</h5>

														<div class="userDate">
															<small>by: <%=rd.getUserBean().getName()%></small><br>
															<small>added: <%=formatted %></small>

														</div>


													</div>

												</a>

											</div>


											<%
								}
							%>
											<%
								}
							%>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>

		</div>


		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>
	<script src="./script/main.js"></script>
</body>
</html>