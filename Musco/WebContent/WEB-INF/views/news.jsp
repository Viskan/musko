<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/news.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<%@ page import="se.nackademin.model.User"%>
<%User user = (User)session.getAttribute("member"); %>
</head>
<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Artist"%>
<%@ page import="se.nackademin.model.Review"%>
<%@ page import="se.nackademin.model.Rating"%>
<%
	@SuppressWarnings("unchecked")
	List<Artist> artistList = (List<Artist>) request.getAttribute("artistList");
	@SuppressWarnings("unchecked")
	List<Review> reviewList = (List<Review>) request.getAttribute("reviewList");
	@SuppressWarnings("unchecked")
	List<Rating> ratingList = (List<Rating>) request.getAttribute("ratingList");
%>


<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<!-- Main component for a primary marketing message or call to action -->
		<h1>
			News<small> The latest from Rolling Stone & Musko.</small>
		</h1>
		<hr>
		<br>
		<div class="row">
			<div class="col-sm-4">
				<img class="img-responsive"
					src="https://rasica.files.wordpress.com/2013/09/rolling_stone-logo1.jpg"
					alt="No picture">
			</div>
			<div class="col-sm-4"></div>
			<div class="col-sm-4"><br>
				<img id="muskoLogo" class="img-responsive" src="img/musko.png"
					alt="No picture">
			</div>
		</div>



		<div class="row">
			<div class="col-sm-8">
				<script language="JavaScript"
					src="http://www.feedroll.com/rssviewer/feed2js.php?src=http%3A%2F%2Fwww.rollingstone.com%2Fmusic.rss&utf=y&pc=y&html=a"
					charset="UTF-8" type="text/javascript"></script>
			</div>
			<div class="col-sm-4">
				<div class="panel with-nav-tabs panel-success">
					<div class="panel-heading">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#newArtists" data-toggle="tab">New
									Artists</a></li>
							<li><a href="#newReviews" data-toggle="tab">New Reviews</a></li>
							<li><a href="#newRatings" data-toggle="tab">New Ratings</a></li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane fade in active" id="newArtists">
							<%for(Artist a : artistList){ %>
<a class="newArtistLink" href="Artist?byid=<%=a.getId()%>"><h3 class="ArtistListName"><%=a.getName()%></h3><img src="<%=a.getImage() %>" class="img-thumbnail" alt="No pic!">
								</a><%} %>
								</div>
							<div class="tab-pane fade" id="newReviews">
							<%for(Review r : reviewList){ %>
							<a class="newArtistLink" href="Review?byid=<%=r.getId()%>"><div class="row">
							<div class="col-sm-8"><%=r.getUserBean().getName()%> wrote a review on <%=r.getAlbumBean().getName()%> by <%=r.getAlbumBean().getArtistBean().getName()%></div>
							<div class="col-sm-4"><img class="img-responsive" src="<%=r.getAlbumBean().getImage()%>"></div>
							</div></a><hr>
							<%}%>
</div>
							<div class="tab-pane fade" id="newRatings">
								<%for(Rating r : ratingList){ %>
								<a class="newArtistLink" href="Album?byid=<%=r.getAlbumBean().getId()%>"><div class="row">
								<div class="col-sm-3"><h2><%=r.getRating()%></h2></div>
								<div class="col-sm-5"><h5>by <%=r.getUserBean().getName()%></h5><h5>on <%=r.getAlbumBean().getName()%></h5></div>
								<div class="col-sm-4"><img class="img-responsive" src="<%=r.getAlbumBean().getImage()%>"></div>
								</div></a><hr>
								
								<%}%>

</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<noscript>
			<a
				href="http://www.feedroll.com/rssviewer/feed2js.php?src=http%3A%2F%2Fwww.rollingstone.com%2Fmusic.rss&utf=y&pc=y&html=y">View
				RSS feed</a>
		</noscript>
		<script src="./script/news.js"></script>


		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>
</body>
</html>