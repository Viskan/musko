<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/review.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Review"%>
<%@ page import="se.nackademin.model.Genre"%>
<%@ page import="se.nackademin.model.ArtistGenre"%>
<%@ page import="se.nackademin.model.Artist"%>
<%@ page import="se.nackademin.model.Album"%>
<%@ page import="se.nackademin.model.User"%>
<%@ page import="se.nackademin.model.Comment"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%User user = (User)session.getAttribute("member"); %>
</head>
<body>
<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		
		<% Review review = (Review)request.getAttribute("review"); 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");%>
		<br>
		<div class="row">
				<div class="col-md-3">
					<div class="thumbnail">
						<img src="<%=review.getAlbumBean().getImage() %>" alt="">
					</div>
					<iframe src="https://embed.spotify.com/?uri=spotify%3Aalbum%3A<%=review.getAlbumBean().getSpotifyId()%>" width="260" height="330" frameborder="0" allowtransparency="true"></iframe>	
				</div>
				<div class="col-md-9"><h1><a class="reviewLinks" href="Album?byid=<%=review.getAlbumBean().getId()%>"><%=review.getAlbumBean().getName()%></a></h1>
				<span class="h3"><a class="reviewLinks" href="Artist?byid=<%=review.getAlbumBean().getArtistBean().getId()%>"><%=review.getAlbumBean().getArtistBean().getName()%> </a></span><span class="h4"><%=sdf.format(review.getAlbumBean().getReleaseDate())%></span>
					<br><br>
					<div class="panel panel-success" id="albumPanel">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Review by <a class="reviewLinks" href="Profile?profile=<%=review.getUserBean().getName()%>"><%=review.getUserBean().getName() %></a></strong><span class="pull-right"><%=sdf.format(review.getDate()) %> </span></h3>
						</div>
							<div class="panel-body">
							
						 	
								<%=review.getText().replace("\n", "<br>") %>
					</div>
					
					
				</div>
			</div>
			</div>
		<div class="row"><div class="col-md-3"></div>
		<div class="col-md-9">
		
		<%if(user != null){ %>
		<form role="form" action="Review" method="post">
		<span class="h4"><span class="label label-success">Comment on this review</span></span>	
		<button type="submit" class="btn btn-success btn-xs pull-right">Post comment <span class="glyphicon glyphicon-play"></span></button>
		<br><br>
		
		<textarea id="commentText" class="form-control" rows="6" name="commentText" required></textarea><br>
		<input type="hidden" class="form-control" name="onReview" value="<%=review.getId()%>">
		</form>
		<%}else{ %>
		<span class="h4"><span class="label label-success">Comment on this review</span></span>	
		<br><br>
		<textarea class="form-control" rows="6"  disabled>You have to be logged in to comment...</textarea>
		<%} %>
		</div></div>
		<div class="row">
		<div class="col-md-3"><br>
		<%   @SuppressWarnings("unchecked")
	  List<Comment> comments = (List<Comment>) request.getAttribute("comments");%>
	<h1><span class="label label-warning">Comments</span></h1>
		</div>
		
		<div class="col-md-9">
		<hr>
		<ul class="media-list">
  
  <%
	  
	
  if(!comments.isEmpty()){ 
	  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  for(Comment c : comments){
  %>
  <li class="media">
    <div class="media-left">
      <a href="Profile?profile=<%=c.getUserBean().getName()%>">
        <img class="media-object commentPic" src="<%=c.getUserBean().getImage()%>" alt="No pic">
      </a>
    </div>
    <div class="media-body">
      <h4 class="media-heading commentHeading"><%=c.getUserBean().getName()%><small> - <%=format.format(c.getDate())%></small></h4>
      <%=c.getText()%>
    </div>
  </li><hr>
  
  <%}}else{%>
  
  <li class="media">
    <div class="media-left">
      <a href="#">
        <img class="media-object commentPic" src="http://upload.wikimedia.org/wikipedia/commons/3/37/No_person.jpg" alt="No pic">
      </a>
    </div>
    <div class="media-body">
      <h4 class="media-heading commentHeading">No comments</h4>
      Be the first to comment!
    </div>
  </li><hr>
	 
  <% }%>
</ul>
		
		
		
		
		</div>
		</div>	
		
		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>
</body>
</html>