<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link href="./css/carousel.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<style>
</style>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<%@ page import="se.nackademin.model.User"%>
<%
	User user = (User) session.getAttribute("member");
%>
<%@ page import="se.nackademin.model.Review"%>
<%@ page import="se.nackademin.model.Comment"%>
<%@ page import="java.util.List"%>
</head>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>

					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>
		<%
			List<Review> reviewList = (List<Review>) request
					.getAttribute("reviewList");
			List<Comment> commentList = (List<Comment>) request
					.getAttribute("commentList");
		%>
									<%
								if (request.getAttribute("loginerror") != null) {
							%>

							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<strong>Nope!</strong> Invalid user information
							</div> <%
 	}
 %>


		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Recent Reviews</h3>
					</div>
					<div class="panel-body">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								<li data-target="#myCarousel" data-slide-to="1" class=""></li>
								<li data-target="#myCarousel" data-slide-to="2" class=""></li>
							</ol>
							<div class="carousel-inner" role="listbox">
								<div class="item active">
									<img class="first-slide carousel-img"
										src="<%=reviewList.get(0).getAlbumBean().getImage()%>"
										alt="First slide">
									<div class="container">
										<div class="carousel-caption">
											<h1><%=reviewList.get(0).getAlbumBean().getName()%></h1>
											<h3><%=reviewList.get(0).getAlbumBean().getArtistBean()
					.getName()%></h3>
											<p>
												by:
												<%=reviewList.get(0).getUserBean().getName()%></p>
											<%
												if (reviewList.get(0).getText().length() > 200) {
											%>
											<p><%=reviewList.get(0).getText().substring(0, 200)%>...
											</p>
											<%
												} else {
											%>
											<p><%=reviewList.get(0).getText()%></p>
											<%
												}
											%>
											<p>
												<a class="btn btn-lg btn-primary"
													href="Review?byid=<%=reviewList.get(0).getId()%>"
													role="button">Read Review</a>
											</p>
										</div>
									</div>
								</div>
								<div class="item">
									<img class="second-slide carousel-img"
										src="<%=reviewList.get(1).getAlbumBean().getImage()%>"
										alt="Second slide">
									<div class="container">
										<div class="carousel-caption">
											<h1><%=reviewList.get(1).getAlbumBean().getName()%></h1>
											<h3><%=reviewList.get(1).getAlbumBean().getArtistBean()
					.getName()%></h3>
											<p>
												by:
												<%=reviewList.get(1).getUserBean().getName()%></p>
											<%
												if (reviewList.get(1).getText().length() > 200) {
											%>
											<p><%=reviewList.get(1).getText().substring(0, 200)%>...
											</p>
											<%
												} else {
											%>
											<p><%=reviewList.get(1).getText()%></p>
											<%
												}
											%>
											<p>
												<a class="btn btn-lg btn-primary"
													href="Review?byid=<%=reviewList.get(1).getId()%>"
													role="button">Read Review</a>
											</p>
										</div>
									</div>
								</div>
								<div class="item">
									<img class="third-slide carousel-img"
										src="<%=reviewList.get(2).getAlbumBean().getImage()%>"
										alt="Third slide">
									<div class="container">
										<div class="carousel-caption">
											<h1><%=reviewList.get(2).getAlbumBean().getName()%></h1>
											<h3><%=reviewList.get(2).getAlbumBean().getArtistBean()
					.getName()%></h3>
											<p>
												by:
												<%=reviewList.get(2).getUserBean().getName()%></p>
											<%
												if (reviewList.get(2).getText().length() > 200) {
											%>
											<p><%=reviewList.get(2).getText().substring(0, 200)%>...
											</p>
											<%
												} else {
											%>
											<p><%=reviewList.get(2).getText()%></p>
											<%
												}
											%>
											<p>
												<a class="btn btn-lg btn-primary"
													href="Review?byid=<%=reviewList.get(2).getId()%>"
													role="button">Read Review</a>
											</p>
										</div>
									</div>
								</div>
							</div>
							<a class="left carousel-control" href="#myCarousel" role="button"
								data-slide="prev"> <span
								class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a> <a class="right carousel-control" href="#myCarousel"
								role="button" data-slide="next"> <span
								class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
				<!-- End of col-md-6 class -->
			</div>
			<div class="col-md-6">
				<div class="panel panel-success" id="latest-comments">
					<div class="panel-heading">
						<h3 class="panel-title">Latest Comments</h3>
					</div>
					<div class="panel-body">
						<%
							if (commentList != null) {
						%>
						<%
							for (Comment c : commentList) {
						%>

						<div class="col-lg-4" id="latest-comments-inner">
							<img class="img-circle" src="<%=c.getUserBean().getImage()%>"
								alt="Generic placeholder image" width="140" height="140"></img>
							<div class="caption">
								<a href="Profile?profile=<%=c.getUserBean().getName()%>"><small>by
										<%=c.getUserBean().getName()%></small></a>
								<h4 id="album-name-comment">
									<a href="Review?byid=<%=c.getReviewBean().getId()%>"><%=c.getReviewBean().getAlbumBean().getName()%></a>
								</h4>
								<p id="comment-text"><%=c.getText()%></p>
							</div>
						</div>

						<%
							}
						%>
						<%
							}
						%>
					</div>

				</div>
				<!-- End of row class -->
			</div>
		</div>



		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>
</body>
</html>