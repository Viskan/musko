<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/artist.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Genre"%>
<%@ page import="se.nackademin.model.ArtistGenre"%>
<%@ page import="se.nackademin.model.Artist"%>
<%@ page import="se.nackademin.model.Album"%>
<%@ page import="se.nackademin.model.Rating"%>
<%@ page import="se.nackademin.model.User"%>
<%@ page import="se.nackademin.model.FavBand"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.util.Comparator"%>
<%@ page import="java.util.Collections"%>
<%User user = (User)session.getAttribute("member"); %>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<nav class="navbar navbar-sm" id="underNav">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav" id="navbarNav">
						<li><a href="Genre">Browse by Genre</a></li>
					</ul>
					<form class="navbar-form" role="search" method="post"
						action="Search">
						<div class="form-group">
							<input type="text" class="form-control" name="search_artist"
								id="search_artist" placeholder="Search Artist">
						</div>
						<button type="submit" class="btn btn-default">
							<span class="glyphicon glyphicon-search" aria-hidden="true"
								aria-label=""></span>
						</button>
					</form>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>

		<!-- Main component for a primary marketing message or call to action -->
		<% se.nackademin.model.Artist artist = (Artist)request.getAttribute("artist"); %>
		<%List<ArtistGenre> genreList= artist.getArtistGenres(); %>
		<%List<Album> albumList = (List<Album>) request.getAttribute("albums");
		  List<Artist> artistsByGenre = (List<Artist>) request.getAttribute("artistsByGenre");
		%>
		<%
			Calendar cal = Calendar.getInstance();
			List<FavBand> favBands = (List<FavBand>) request.getAttribute("favBands");
		%>
		<div class="row">
			<div class="col-md-3">
				<div class="thumbnail">
					<img src="<%=artist.getImage() %>" alt="No image">
					<h3 id="fans">
						<span class="glyphicon glyphicon-heart"><%=favBands.size() %></span>
					</h3>

					<form role="form" id="favBandsFrom" action="Artist" method="post">
						<%if(user != null){ 
							boolean oldFav = false;
						for(FavBand fb : favBands){
							if(fb.getUserBean().getName().equals(user.getName())){%>
						<span class="h5">Remove from favourites</span><span class="h3"><span
							onclick="addDeleteFav()" id="deleteFav"
							class="glyphicon glyphicon-remove pull-right"></span></span> <input
							type="hidden" class="form-control" value="<%=artist.getId()%>"
							name="favArtist"> <input type="hidden"
							class="form-control" value="delete" name="addOrDelete">

						<% oldFav = true;
							break;}else{ oldFav = false;%>
						<% }}%>

						<%if(oldFav == false){ %>
						<span class="h5">Add to favourites</span><span class="h3"><span
							onclick="addDeleteFav()" id="addFav"
							class="glyphicon glyphicon-ok pull-right"></span></span> <input
							type="hidden" class="form-control" value="<%=artist.getId()%>"
							name="favArtist"> <input type="hidden"
							class="form-control" value="add" name="addOrDelete">


						<%}} %>
					</form>
				</div>


				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Similar Artists</h3>
							</div>

							<%if (artistsByGenre != null){%>

							<ul class="list-group">
								<% for(Artist a: artistsByGenre){%>
								  <%if(artist.getName().equals(a.getName())){continue;} %>
									<a href="Artist?byid=<%=a.getId()%>" class="list-group-item"><strong><%=a.getName()%></strong></a>

								<%	}%>
							</ul>
							<% }%>

						</div>
					</div>
				</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">Spotify Player</h3>
						</div>
						<div id="spotifyPlayer" class="panel-body">
							<img class="img-responsive"
								src="http://www.hypebot.com/.a/6a00d83451b36c69e2017d3ce16f18970c-pi"
								alt="No pic">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<%--hejhej --%>
		<div class="col-md-9">
			<div class="panel panel-success" id="albumPanel">
				<div class="panel-heading">
					<h1 class="panel-title">
						<strong><%=artist.getName() %></strong>
					</h1>
				</div>
				<div class="panel-body">
					<%if(genreList != null){ %>
					<%for(ArtistGenre a: genreList){ %>
					<span class="label label-info"><a
						href="Genre?genre=<%=a.getGenreBean().getName() %>&letter=<%=a.getGenreBean().getName().substring(0, 1) %>"><strong><%=a.getGenreBean().getName() %></strong></a></span>
					<%} %>
					<%} %>
					<br> <br>
					<div id="artist-description">
						<p><%=artist.getDescription().replace("\n", "<br>") %></p>

					</div>
					<div class="row">
						<div class="col-md-11"></div>
						<div class="col-md-1">
							<button type="button" class="btn btn-info btn-xs"
								id="more-button">more..</button>
						</div>

					</div>
				</div>

			</div>
			<div class="row">

				<div class="col-md-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">Albums</h3>
						</div>
						<div class="panel-body">
							<div class="row">
							  <%if(albumList != null){ %>
								<%for(Album a: albumList){ %>
								<%cal.setTime(a.getReleaseDate()); 
								List<Rating> ratings = a.getRatings();
								//(List<Rating>)request.getAttribute("ratings")
								%>

								<div class="col-sm-6 col-md-3 albumThumbs">
									<div class="thumbnail albumThumbsThumbs">

										<a href="Album?byid=<%=a.getId()%>"><img
											src="<%=a.getImage().toString()%>" class="albumThumbImg"></a>
										<div class="caption">
											<div class="row">
												<div class="col-md-12">
													<p><%=a.getName() %><span id="playButton"
															data-spotify="<%=a.getSpotifyId() %>"
															onclick="playAlbum(this)"
															class="glyphicon glyphicon-play pull-right"></span>
													</p>
													<!-- Reviewknapp -->
													<%if (user!= null){ %>
													<a class="reviewButton"
														href="AddReview?byid=<%=a.getId()%>"
														title="Write a review"> <span
														class="glyphicon glyphicon-pencil pull-right"></span></a>
													<%} %>
													<!-- /Reviewknapp -->
													<small><%=cal.get(Calendar.YEAR) %></small>


												</div>
												<div class="col-md-12">
													<%
												int ratingSum = 0;
												int avgRating = 0;
												for(Rating r: ratings){ 
													ratingSum += r.getRating();
												}
												if(ratings.isEmpty()){avgRating = 0;}else{avgRating = ratingSum/ratings.size();}%>
													<h5 style="color: red; margin: 0px; padding: 0px;"><%=avgRating %>/100
														<small>ratings <%=ratings.size() %></small>
													</h5>

												</div>



											</div>
										</div>
									</div>
								</div>
								<%} %>
								<%} %>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>

	</div>


	<hr>
	<footer>
		<p>
			&copy; MUSKO. 2015 <small>The best site in the
				world</small>
		</p>
	</footer>
	</div>
	<script src="./script/main.js"></script>
</body>
</html>