<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Genre"%>
<%@ page import="se.nackademin.model.ArtistGenre"%>
<%@ page import="se.nackademin.model.User"%>
<%User user = (User)session.getAttribute("member"); %>
</head>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>
				<nav class="navbar navbar-sm" id="underNav">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav" id="navbarNav">
						<li><a href="Genre">Browse by Genre</a></li>
					</ul>
					<form class="navbar-form" role="search" method="post" action="Search">
							<div class="form-group">
								<input type="text" class="form-control"
									placeholder="Search Artist" name="search_artist">
							</div>
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-search" aria-hidden="true"
									aria-label="Register"></span>
							</button>
						</form>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>

		<div class="jumbotron">
			<h1>Browse by Genre</h1>
			<nav>
			
				<ul class="pagination">
					<li><a href="Genre?letter=sign">#</a></li>
					<li><a href="Genre?letter=a">A</a></li>
					<li><a href="Genre?letter=b">B</a></li>
					<li><a href="Genre?letter=c">C</a></li>
					<li><a href="Genre?letter=d">D</a></li>
					<li><a href="Genre?letter=e">E</a></li>
					<li><a href="Genre?letter=f">F</a></li>
					<li><a href="Genre?letter=g">G</a></li>
					<li><a href="Genre?letter=h">H</a></li>
					<li><a href="Genre?letter=i">I</a></li>
					<li><a href="Genre?letter=j">J</a></li>
					<li><a href="Genre?letter=k">K</a></li>
					<li><a href="Genre?letter=l">L</a></li>
					<li><a href="Genre?letter=m">M</a></li>
					<li><a href="Genre?letter=n">N</a></li>
					<li><a href="Genre?letter=o">O</a></li>
					<li><a href="Genre?letter=p">P</a></li>
					<li><a href="Genre?letter=q">Q</a></li>
					<li><a href="Genre?letter=r">R</a></li>
					<li><a href="Genre?letter=s">S</a></li>
					<li><a href="Genre?letter=t">T</a></li>
					<li><a href="Genre?letter=u">U</a></li>
					<li><a href="Genre?letter=v">V</a></li>
					<li><a href="Genre?letter=W">W</a></li>
					<li><a href="Genre?letter=x">X</a></li>
					<li><a href="Genre?letter=y">Y</a></li>
					<li><a href="Genre?letter=z">Z</a></li>
				</ul>
			</nav>
			<% List<Genre> genres = (List<Genre>)request.getAttribute("genres");
				if(genres == null){%>
					<h1>NULL</h1>			
				<%}else{ %>
				<div class="row">
				 	<div class="col-md-4">
					<% for(Genre g: genres){%>
					<table>
						<tr>
							<td>
							   <a href="Genre?genre=<%= g.getName()%>&letter=<%=request.getParameter("letter")%>">
							      <span class="label label-default"><%= g.getName() %></span>
							   </a>
							 </td>
						</tr>
					</table>
					<%} %>
					</div>
					<% List<ArtistGenre> artist = (List<ArtistGenre>)request.getAttribute("artistByGenre"); %>
					<%if(artist != null) {%>
						<div class="col-md-8">
							<table>
								<%for(ArtistGenre a: artist){ %>
									<tr>
										<td>
											<span class="label label-info"><a href="Artist?byid=<%=a.getArtistBean().getId()%>">
												<%=a.getArtistBean().getName()%>
											</a></span>
										 </td>
										</tr>
								<%} %>
							</table>
						 </div>
					 <%}if(request.getAttribute("emptyGenre") != null){ %>
						 <div class="col-md-8">
						 	<h3><span class="label label-warning"><%= request.getAttribute("emptyGenre")%></span></h3>
						 </div>
					 <%} %>
				<%} %>
				</div>
		</div>


		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>

</body>
</html>