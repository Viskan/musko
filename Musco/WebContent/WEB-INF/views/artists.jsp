<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/reviews.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Artist"%>
<%@ page import="se.nackademin.model.User"%>
<%User user = (User)session.getAttribute("member"); %>
</head>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false">Dropdown
								<span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li><a href="#">One more separated link</a></li>

							</ul></li>
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>

							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li class="dropdown-header">Nav header</li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<nav class="navbar navbar-sm" id="underNav">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav" id="navbarNav">
						<li><a href="Genre">Browse by Genre</a></li>
					</ul>		
						<form class="navbar-form" role="search" method="post" action="Search">
							<div class="form-group">
								<input type="text" class="form-control" name="search_artist" id="search_artist" placeholder="Search Artist">
							</div>
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-search" aria-hidden="true"
									aria-label=""></span>
							</button>
						</form>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>

		<!-- Main component for a primary marketing message or call to action -->

			<div class="row">
			<%List<Artist> list = (List<Artist>)request.getAttribute("artistList"); %>

				<div class="col-md-12">
					<div class="panel panel-success" id="">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Recently Added Artists</strong></h3>
						</div>
							<div class="panel-body">
							<div class="row">
							<div class="col-md-12">
								<%for(Artist a: list){ %>
							
								<div class="col-md-3 col-sm-3 albumThumbs" id="artists-thumb">
									<a class="thumbnail" id="artists-caption" href="Artist?byid=<%=a.getId()%>">
										<img alt="artist picture" src="<%=a.getImage()%>" id="artist-picture"></img>
										<div class="caption">
											<h5><strong><%=a.getName() %></strong></h5>
										</div>
									</a>
								</div>
								<%} %>
								</div>
							</div>
							</div>
					</div>
					
					
				</div>
			</div>

		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>
	
	<script src="./script/search.js"></script>
</body>
</html>