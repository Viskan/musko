<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/spotify.css">
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<%@ page import="se.nackademin.model.User"%>
<%User user = (User)session.getAttribute("member"); %>
</head>
<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Albumtype"%>
<% @SuppressWarnings("unchecked")
		List<Albumtype> albumTypes = (List<Albumtype>) request.getAttribute("albumTypes");%>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<!-- content -->
		<h1>
			Artists & Albums <small>Add, Edit.</small>
		</h1>
		<form action="#">
			<div class="form-group">
				<label for="searchArtist">Search artist</label> <input type="text"
					class="form-control" id="searchArtist" placeholder="Search artist">
			</div>
		</form>
		<div class="row">
			<div class="col-md-6">
				<ul id="artistList" class="list-group">

				</ul>

			</div>
			<div class="col-md-6">
				<div id="viewArtist" class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">View Artist</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-7">
								<img class="img-responsive" id="viewArtistPic" src=""
									alt="No picture">
							</div>

							<div class="col-md-5">
								<h3 id="viewArtistName"></h3>
								<h5 id="viewArtistSpotifyID"></h5>
								<h5 id="viewArtistSpotifyFollowers"></h5>
								<ul class="list-unstyled" id="viewArtistGenres"></ul>
								<div class="btn-group btn-group-sm" role="group"
									aria-label="...">
									<button type="button" class="btn btn-success"
										onclick="addEditArtist()" data-toggle="modal"
										data-target="#artistModal">Add / Edit</button>
									<button onclick="viewAlbums()" type="button"
										class="btn btn-warning">View Albums</button>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">View Albums</h3>
			</div>

			<div class="panel-body">
				<h5 id="viewAlbumsTotal"></h5>
				<br> <br>
				<div id="viewAlbums" class="row"></div>

			</div>
			<!-- panel-content -->
		</div>
		<!-- panel -->


		<!-- /Pagecontent -->
		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>


		<!-- ArtistModal -->
		<div id="artistModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div id="artistModalHeader" class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							Add / Edit <small>Artist</small>
						</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">
								<img id="artistModalImage" class="img-responsive" src=""
									alt="No Picture">
							</div>
						</div>
						<br>
						<form id="artistModalForm" class="form-horizontal" role="form" action="http://localhost:8080/Musco/resources/spotifyRest/addArtist" method="post">
							<div class="form-group">
								<label class="control-label col-sm-2" for="artistModalName">Name:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="artistModalName" name="artistModalName"
										placeholder="ooops">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="artistModalId">ID:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="artistModalId" 
										placeholder="This is a new artist" disabled>
										<input type="hidden" class="form-control" id="artistModalIdHidden" name="artistModalId">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="artistModalSpotifyId">SpotifyID:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										id="artistModalSpotifyId" placeholder="ooops" disabled>
										<input type="hidden" class="form-control"
										id="artistModalSpotifyIdHidden" name="artistModalSpotifyId">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="artistModalImageUrl">Image
									url:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										id="artistModalImageUrl" name="artistModalImageUrl" placeholder="no pic">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="artistModalBio">Description:</label>
								<div class="col-sm-10">
									<textarea class="form-control" rows="5" id="artistModalBio" name="artistModalBio"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<label class="control-label" for="artistModalGenres">Genres:</label>
								</div>
								<div class="col-sm-10" id="artistModalGenres" name="artistModalGenres"></div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<br>
									<button onclick="addGenres()" type="button"
										class="btn btn-warning btn-xs">Add Genre</button>
								</div>
								<div class="col-sm-10">
								<br>
									<select class="form-control" id="artistModalGenresList">
										<option>Alternative Pop/Rock</option>
										<option>Blues</option>
										<option>Club/Dance</option>
										<option>Computer Music</option>
										<option>Country</option>
										<option>Dancehall</option>
										<option>Death Metal/Black Metal</option>
										<option>Disco</option>
										<option>Electro</option>
										<option>Emo</option>
										<option>Experimental</option>
										<option>Film Music</option>
										<option>Folk</option>
										<option>Funk</option>
										<option>Gangsta Rap</option>
										<option>Gay</option>
										<option>Glam Rock</option>
										<option>Grunge</option>
										<option>Hard Rock</option>
										<option>Heavy Metal</option>
										<option>Hip-Hop</option>
										<option>Jazz</option>
										<option>Metal</option>
										<option>Pop</option>
										<option>Psychedelic</option>
										<option>Punk</option>
										<option>Rap</option>
										<option>Reggae</option>
										<option>Rock</option>
										<option>Rockabilly</option>
										<option>Singer/Songwriter</option>
										<option>Soul</option>
										<option>Synth Pop</option>
										<option>Techno</option>
										<option>Thrash</option>
										<option>Video Game Music</option>
									</select>
								</div>
							</div>
							<div class="row">
							<div class="col-sm-2"></div>
								<div class="col-sm-10"><br>
							<button id="artistModalSubmit" type="submit" class="btn btn-success btn-lg pull-right">Add/Edit Artist</button></div>
							</div>
							</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /ArtistModal -->

<!--AlbumModal  -->
<div id="albumModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div id="albumModalHeader" class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							Add / Edit <small>Artist</small>
						</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">
								<img id="albumModalImage" class="img-responsive" src=""
									alt="No Picture">
							</div>
						</div>
						<br>
						<form id="albumModalForm" class="form-horizontal" role="form" action="http://localhost:8080/Musco/resources/spotifyRest/addAlbum" method="post">
							<div class="form-group">
								<label class="control-label col-sm-2" for="albumModalName">Name:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="albumModalName" name="albumModalName"
										placeholder="ooops">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="albumModalId">ID:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="albumModalId" 
										placeholder="This is a new album" disabled>
										<input type="hidden" class="form-control" id="albumModalIdHidden" name="albumModalId">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="albumModalSpotifyId">SpotifyID:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										id="albumModalSpotifyId" placeholder="ooops" disabled>
										<input type="hidden" class="form-control"
										id="albumModalSpotifyIdHidden" name="albumModalSpotifyId">
										<input type="hidden" class="form-control"
										id="albumModalArtistSpotifyId" name="albumModalArtistSpotifyId">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="albumModalReleaseDate">Release date:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										id="albumModalReleaseDate" name="albumModalReleaseDate" placeholder="0000-00-00">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="albumModalAlbumType">Album type:</label>
								<div class="col-sm-10">
									<select class="form-control" id="albumModalAlbumType" name="albumModalAlbumType">
									<%for(Albumtype a : albumTypes){ %>
									<option><%=a.getName() %></option>
									<%} %>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="albumModalImageUrl">Image
									url:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										id="albumModalImageUrl" name="albumModalImageUrl" placeholder="no pic">
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-2">
									<label class="control-label" for="albumModalGenres">Genres:</label>
								</div>
								<div class="col-sm-10" id="albumModalGenres" name="albumModalGenres"></div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<br>
									<button onclick="addAlbumGenres()" type="button"
										class="btn btn-warning btn-xs">Add Genre</button>
								</div>
								<div class="col-sm-10">
								<br>
									<select class="form-control" id="albumModalGenresList">
										<option>Alternative Pop/Rock</option>
										<option>Blues</option>
										<option>Club/Dance</option>
										<option>Computer Music</option>
										<option>Country</option>
										<option>Dancehall</option>
										<option>Death Metal/Black Metal</option>
										<option>Disco</option>
										<option>Electro</option>
										<option>Emo</option>
										<option>Experimental</option>
										<option>Film Music</option>
										<option>Folk</option>
										<option>Funk</option>
										<option>Gangsta Rap</option>
										<option>Gay</option>
										<option>Glam Rock</option>
										<option>Grunge</option>
										<option>Hard Rock</option>
										<option>Heavy Metal</option>
										<option>Hip-Hop</option>
										<option>Jazz</option>
										<option>Metal</option>
										<option>Metalcore</option>
										<option>Pop</option>
										<option>Psychedelic</option>
										<option>Punk</option>
										<option>Rap</option>
										<option>Reggae</option>
										<option>Rock</option>
										<option>Rockabilly</option>
										<option>Singer/Songwriter</option>
										<option>Sludge Metal</option>
										<option>Soul</option>
										<option>Synth Pop</option>
										<option>Techno</option>
										<option>Thrash</option>
										<option>Video Game Music</option>
									</select>
								</div>
							</div>
							<div class="row">
							<div class="col-sm-2"></div>
								<div class="col-sm-10"><br>
							<button id="albumModalSubmit" type="submit" class="btn btn-success btn-lg pull-right">Add/Edit Album</button></div>
							</div>
							</form>
					</div>
				</div>
			</div>
		</div>
		<!--/AlbumModal  --> 












	</div>
	<!-- container -->
	<script src="./script/spotify.js"></script>
</body>
</html>