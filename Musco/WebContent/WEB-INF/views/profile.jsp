<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MUSKO. | music community</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/profile.css">

<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<%@ page import="se.nackademin.model.User"%>
<%@ page import="java.util.List"%>
<%@ page import="se.nackademin.model.Review"%>
<%@ page import="se.nackademin.model.Comment"%>
<%@ page import="se.nackademin.model.FavBand"%>
<%@ page import="se.nackademin.model.Rating"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%
	User user = (User) session.getAttribute("member");
	List<Review> reviews = (List<Review>) request
			.getAttribute("reviews");
	List<Comment> comments = (List<Comment>) request
			.getAttribute("comments");
	User thisProfile = (User) request.getAttribute("thisProfile");
	List<FavBand> favBands = (List<FavBand>) request
			.getAttribute("favBands");
	List<Rating> ratings = (List<Rating>) request.getAttribute("ratings");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
%>
</head>
<body>

	<div class="container">

		<nav id="headMenu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<a href="MainPage"><img class="img-responsive"
						src="img/musko.png" alt="musko"></a>
					<ul class="nav navbar-nav">

						<li><a href="Reviews">Reviews</a></li>
						<li><a href="Artists">Artists</a></li>
						<li><a href="News">News</a></li>
						<li><a href="TopList">Top20</a></li>
						
						<li>
							<%
								if (request.getAttribute("loginerror") != null) {
							%> Invalid user information <%
								}
							%>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<div id="navbar" class="navbar-collapse collapse">

							<%
								if (session.getAttribute("member") != null) {
							%>


							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-expanded="false"><%=user.getName()%><span
										class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="Profile">Profile</a></li>
										<li><a href="logout">Log Out</a></li>
										<li class="divider"></li>
										<li><a href="Settings">Settings</a></li>
									</ul></li>
							</ul>
							<%
								} else {
							%>
							<form class="navbar-form navbar-right" method="post"
								action="login">
								<div class="form-group">
									<input type="text" placeholder="Username" class="form-control"
										name="username">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password"
										class="form-control" name="password">
								</div>
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-play" aria-hidden="true"
										aria-label="Sign in"></span>
								</button>
								<label class="btn btn-warning"> <a href="Register">
										<span class="glyphicon glyphicon-user" aria-hidden="true"
										aria-label="Register"><small>+</small></span>
								</a>
								</label>
							</form>

							<%
								}
							%>
						</div>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</nav>

		<!-- Content -->
		<br>
		<div class="row">
			<div class="col-sm-3">
				<img class="img-responsive img-rounded"
					src="<%=thisProfile.getImage()%>">
				<h1 id="profileName"><%=thisProfile.getName()%></h1>
				<%
					if (thisProfile.getMail() != null) {
				%>
				<h5>
					<em><%=thisProfile.getMail()%></em>
				</h5>
				<%
					}
				%>
				<h4>
					Ratings <span class="badge profileBadge"><%=ratings.size()%></span>
				</h4>
				<h4>
					Reviews <span class="badge profileBadge"><%=reviews.size()%></span>
				</h4>
				<h4>
					Comments <span class="badge profileBadge"><%=comments.size()%></span>
				</h4>
				<h4>
					Favourite Artists <span class="badge profileBadge"><%=favBands.size()%></span>
				</h4>
			</div>
			<div class="col-sm-1"></div>
			<div class="col-sm-8">
				<h3>
					<span class="label label-success">Latest reviews</span>
				</h3>
				<ul class="list-group" id="reviewList">
					<%
						int i = 0;
						for (Review review : reviews) {
							i++;
					%>
					<a href="Review?byid=<%=review.getId()%>"><li
						class="list-group-item"><div class="row">
								<div class="col-sm-5">
									<h5><%=sdf.format(review.getDate())%></h5>
									<h3><%=review.getAlbumBean().getName()%></h3>
									<h4><%=review.getAlbumBean().getArtistBean().getName()%></h4>
								</div>
								<div class="col-sm-2">
									<br> <img class="img-responsive"
										src="<%=review.getAlbumBean().getImage()%>">
								</div>
								<div class="col-sm-4">
									<%
										if (review.getText().length() > 200) {
									%>
									<%=review.getText().substring(0, 200) + "..."%>
									<%
										} else {
									%>
									<%=review.getText()%>
									<%
										}
									%>
								</div>
							</div></li></a>
					<%
						if (i >= 3) {
								break;
							}
						}
					%>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8" id="favBandsDiv">

				<h3>
					<span class="label label-success">Favourite artists</span>
				</h3>
				<br>
				<%
					for (FavBand fav : favBands) {
				%>
				<a href="Artist?byid=<%=fav.getArtistBean().getId()%>"><div
						class="col-sm-3">
						<img class="img-circle favBandsPic"
							src="<%=fav.getArtistBean().getImage()%>">
						<h3><%=fav.getArtistBean().getName()%></h3>
					</div></a>
				<%
					}
				%>
			</div>
			<div class="col-sm-4">
				<h3>
					<span class="label label-success">Recent activity</span>
				</h3>


				<div class="panel with-nav-tabs panel-success">
					<div class="panel-heading">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profileRatings"
								data-toggle="tab">Ratings</a></li>
							<li><a href="#profileComments" data-toggle="tab">Comments</a></li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane fade in active" id="profileRatings">
								<ul class="list-group recentActList">
									<%for(Rating rating: ratings){ %>
									<a href="Album?byid=<%=rating.getAlbumBean().getId()%>"><li class="list-group-item"><div
												class="row">
												<div class="col-sm-3">
													<h1><%=rating.getRating()%></h1>
												</div>
												<div class="col-sm-6">
													<h4><%=rating.getAlbumBean().getName()%></h4>
													<h5><%=rating.getAlbumBean().getArtistBean().getName()%></h5>
												</div>
												<div class="col-sm-3">
													<img class="img-responsive"
														src="<%=rating.getAlbumBean().getImage()%>">
												</div>
											</div></li></a>
									<%}%>
								</ul>

							</div>
							<div class="tab-pane fade" id="profileComments">
							<ul class="list-group recentActList">
								<%for(Comment comment : comments){ %>
								<a href="Review?byid=<%=comment.getReviewBean().getId()%>"><li class="list-group-item">
								<div class="row">
								<div class="col-sm-4"><small><%=sdf.format(comment.getDate())%></small><h6>Review by <%=comment.getReviewBean().getUserBean().getName()%></h6></div>
								<div class="col-sm-5"><%=comment.getText()%></div>
								<div class="col-sm-3"><img class="img-responsive" src="<%=comment.getReviewBean().getAlbumBean().getImage()%>"></div>
								</div></li></a>
								
								
								<%}%>
								</ul>
							</div>

						</div>
					</div>
				</div>


			</div>
		</div>




		<hr>
		<footer>
			<p>
				&copy; MUSKO. 2015 <small>The best site in the
					world</small>
			</p>
		</footer>
	</div>
</body>
<script src="./script/profile.js"></script>
</html>